// This handles the global environment that many pieces of the API use to communicate, 
// Should be loaded ASAP.

var ENV;
ENV = {
	'main': goLive(requestDistinctOnscreenContext()),
	'color': new ColorSource(),
	'renderProvider': new RenderProvider()
};
