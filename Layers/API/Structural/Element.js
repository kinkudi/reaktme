// Element is the basic building block of the GUI
// They can nest, render to canvas, listen for events, and be animated.

// This file also contains the sub-types, Free and Fixed.
// Free has its own onscreen canvas to render to, but is still limited in size by it's optional parent.
// Fixed has an onscreen canvas shared from its parent.

function Element() {
	this.locate() 
	this.privateOffscreenContext = requestPrivateOffscreenContext();
	this.live = 0;
	this.imageloading = 0;
	this.launchable = 0;
	this.waitq = []
	return this;
};
Element.prototype.relocate = function(xpos,ypos,xsz,ysz) {
	return	this.locate(xpos,ypos,xsz,ysz).at(this.onscreenContext);
};
Element.prototype.locate = function(xpos,ypos,xsz,ysz) {
	this.loc = new Locate(xpos,ypos,xsz,ysz);
	this.launchable = 0;
	return this;
};
Element.prototype.at = function(gcontext) {
	this.onscreenContext = gcontext;
	this.giveContext(gcontext);
	this.launchable = 0;
	return this;
};
Element.prototype.addTo = function(receivingParent) {
	this.containingParent = receivingParent;
	receivingParent.add(this);
	return this;
};
Element.prototype.add = function(child) {
	if(this.children == undefined)
		this.children = [];
	if(this.sharedChildContext == undefined)
		this.sharedChildContext = requestPrivateOffscreenContext(this.loc.abs.xsz,this.loc.abs.ysz);
	this.children.push(child);
	child.containingParent = this;
	child.giveContext(this.sharedChildContext);
	this.launchable = 0;
	return this;
};
Element.prototype.giveContext = function(aContext) {
	this.loc.inContext(aContext);
	this.privateOffscreenContext.canvas.width = this.loc.abs.xsz;
	this.privateOffscreenContext.canvas.height = this.loc.abs.ysz;
	this.launchable = 0;
	return this;
};
Element.prototype.warmup = function() {
	if(this.live == 1)
		return this;
	goLive(this.onscreenContext);
	this.live = 1;
	return this;
};
Element.prototype.cooldown = function() {
	if(this.live == 0)
		return this;
	goDark(this.onscreenContext);
	this.live = 0;
	return this;
};
Element.prototype.renders = function(renderer,ondone) {
	if(this.renderers == undefined)
		this.renderers = []
	if(!ondone) {
		if(this.constructor == Free) {
			var element = this;
			ondone = function () {
				element.launch();
			};
		}
		else if(this.constructor == Fixed) {
			var element = this;
			if(this.containingParent) 
				var element = this.containingParent;
			ondone = function () {
				element.launch();
			};
		}
		else {
			alert('Unknown Element type encountered in renders Ondone function creation');
		}
	}
	this.renderers.push({ 'renderer' : renderer, 'ondone' : ondone });
	this.launchable = 0;
	return this;
};
Element.prototype.clear = function() {
	/* or something else */
	this.privateOffscreenContext.save();
	this.privateOffscreenContext.setTransform(1,0,0,1,0,0);
	for(var ci in this.children) {
		var child = this.children[ci];
		child.clear();
	}
	this.privateOffscreenContext.clearRect(0,0,this.loc.abs.xsz,this.loc.abs.ysz);
	this.privateOffscreenContext.restore();
	this.onscreenContext.clearRect(this.loc.abs.x,this.loc.abs.y,this.loc.abs.xsz,this.loc.abs.ysz);
	return this;
};
Element.prototype.draw = function() {
	if(this.launchable == 1)
		return this;
	for(var ri in this.renderers) {
		var rendermsg = this.renderers[ri];
		rendermsg['renderer'].renderTo(this, this.privateOffscreenContext,rendermsg['ondone']);
 	}
	for(var ci in this.children) {
		var child = this.children[ci];
		child.draw();
	}
	this.launchable = 1;
	return this;
};
Element.prototype.launch = function() {
	this.draw();
	if(this.live == 0)
		this.warmup();
	if(this.children && this.children.length > 0) {
		for(var ci in this.children) {
			var child = this.children[ci];
			child.launch();
		}
		this.privateOffscreenContext.drawImage(this.sharedChildContext.canvas,0,0);
	}
	this.onscreenContext.drawImage(this.privateOffscreenContext.canvas,this.loc.abs.x,this.loc.abs.y);
	return this;
};

function Free() {
	this.base.constructor.apply(this);
	this.at(requestDistinctOnscreenContext());
	return this;
};
Free.expands(Element);

function Fixed() {
	this.base.constructor.apply(this,arguments);
	return this;
};
Fixed.expands(Element);
Fixed.prototype.giveContext = function(sharedContextFromParent) {
	this.base.giveContext.call(this,sharedContextFromParent);
	this.onscreenContext = sharedContextFromParent;
	return this;
};
Fixed.prototype.ensureReady = function() {
	if(this.onscreenContext == undefined)
		this.giveContext(ENV.main);
	return this;
};
Fixed.prototype.warmup = function() {
	return this.ensureReady().base.warmup.call(this,arguments);
};
Fixed.prototype.launch = function() {
	return this.ensureReady().base.launch.call(this,arguments);
};
