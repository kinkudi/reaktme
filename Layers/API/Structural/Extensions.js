// This file has 4 sections
// 1. Extensions to String
// 2. Utility functions
// 3. Currying 
// 4. Prototypica Inheritance

// Extensions to String

if(typeof(String.prototype.trim) === "undefined")
{
    String.prototype.trim = function() 
    {
	return String(this).replace(/^\s+|\s+$/g, '');
    };
}
String.prototype.splitf = function (regex) {
	var regexp = new RegExp(regex);
	var match = regexp.exec(this);
	if(!match || match == null)
		return [this];
	var index = this.indexOf(match);
	return [this.slice(0,index),this.slice(index+match.length)]		
};
String.prototype.rjust = function( width, padding ) {
	padding = padding || " ";
	padding = padding.substr( 0, 1 );
	if( this.length < width )
		return padding.repeat( width - this.length ) + this;
	else
		return this;
};
String.prototype.repeat = function( num ) {
	for( var i = 0, buf = ""; i < num; i++ ) buf += this;
		return buf;
};

// Utility Functions 

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};
function toArray(conv) {
	return Array.prototype.slice.call(conv);
};
// Currying
Function.prototype.as = function() {
	if (arguments.length < 1)
		return this;
	var __method = this;
	var args = toArray(arguments);
	return function() {
		var largs = toArray(arguments);
		return __method.apply(this,largs.concat(args));
	}
};
// Prototypical Inheritance
Function.prototype.expands = function( baseClassOrObject ) {
    if ( baseClassOrObject.constructor == Function ) {
        this.prototype = new baseClassOrObject();
        this.prototype.constructor = this;
        this.prototype.base = baseClassOrObject.prototype;
    }
    else {
        this.prototype = baseClassOrObject;
        this.prototype.constructor = this;
        this.prototype.base = baseClassOrObject;
    }
    return this;
};
