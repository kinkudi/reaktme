function Locate(xpos,ypos,xsz,ysz) {
	xpos = xpos || 0.0;
	ypos = ypos || 0.0;
	xsz = xsz || 1.0;
	ysz = ysz || 1.0;
	this.rel = new Relative(xpos,ypos,xsz,ysz);
	return this;
};
Locate.prototype.inContext = function(g) {
	this.abs = new Absolute(this.rel,g);
	return this;
};
function Relative(xpos,ypos,xsz,ysz) {
	this.x = xpos;
	this.y = ypos;
	this.xsz = xsz;
	this.ysz = ysz;
	return this;
};
function Absolute(rel,context) {
	this.maxx = context.canvas.width;
	this.maxy = context.canvas.height;
	//alert('Context w,h! = ' + this.maxx + ',' + this.maxy);
	this.x = Math.round(rel.x * this.maxx);
	this.xsz = Math.round(rel.xsz * this.maxx);
	this.endx = this.x+this.xsz;
	this.y = Math.round(rel.y * this.maxy);
	this.ysz = Math.round(rel.ysz * this.maxy);
	this.endy = this.y+this.ysz;
	return this;
};
