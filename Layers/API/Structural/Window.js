// Defines properties upon the window object that are useful for providing a concise API
// And serving global access to frequently used properties which it is convenient to have globally available.

window._ = function () {
	return new Render();
};
window.Color = ENV.color;
