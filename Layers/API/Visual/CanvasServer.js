function CanvasServer() {
	return this;
};
var GLOBALZINDEX = 0;
function requestNextZIndex() {
	var returnablez = GLOBALZINDEX;
	GLOBALZINDEX += 1;
	return returnablez;
};
var BODY = document.getElementsByTagName('body')[0];
function requestPrivateOffscreenContext(width,height) {
	var xwidth = width || window.innerWidth;
	var yheight = height || window.innerHeight;
	var newc = document.createElement('canvas');
	var newg = newc.getContext('2d');
	newg.canvas.width = xwidth;
	newg.canvas.height = yheight;
	return newg;
};
function attach(context) {
	BODY.appendChild(context.canvas);
	return context;
};
function goLive(context) {
	context.canvas.style.display = 'inline';
	return context;
};
function goDark(context) {
	context.canvas.style.display = 'none';
	return context;
};
function requestDistinctOnscreenContext(width,height) {
	var newprivateoffscreen = requestPrivateOffscreenContext(width,height);
	newprivateoffscreen.canvas.style.position = 'fixed';
	newprivateoffscreen.canvas.style.top = 0;
	newprivateoffscreen.canvas.style.left = 0;
	newprivateoffscreen.canvas.style.zIndex = requestNextZIndex();
	goDark(newprivateoffscreen);	
	return attach(newprivateoffscreen);
};
