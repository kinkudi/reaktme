// ColorSource hadnles the sourcing and provision of all colors.
// This also covers things like gradients, and animation vectors where color changes.

function ColorSource() {
	return this;

};
ColorSource.prototype.orange = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(30,100%,50%,' + alpha + ')';
};
ColorSource.prototype.blue = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(200,100%,50%,' + alpha + ')';
};
ColorSource.prototype.red = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(5,100%,50%,' + alpha + ')';
};
ColorSource.prototype.yellow = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(90,100%,50%,' + alpha + ')';
};
ColorSource.prototype.green = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(130,100%,50%,' + alpha + ')';
};
ColorSource.prototype.black = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(0,0%,0%,' + alpha + ')';
};
ColorSource.prototype.default = function() {
	return this.orange(toArray(arguments));
};
