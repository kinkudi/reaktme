// Render is the front-end of the API (to Render Provider's back end).
// It provides the convenient function chaining method of specifying a series of renders.
// And fully interfaces with everything the 2D Canvas API provides, except in a more efficient, more
// beautiful form.

// It is tightly linked to RenderProvider.
// We should probably untangle this somewhat.

function Render() {
	this.provider = ENV.renderProvider;
	this.renderactions = [];
	return this;
};
Render.prototype.fill = function () {
	this.renderactions.push(new RenderAction('fill'));
	return this;
};
Render.prototype.nofill = function () {
	this.renderactions.push(new RenderAction('nofill'));
	return this;
}
Render.prototype.stroke = function () {
	this.renderactions.push(new RenderAction('stroke'));
	return this;
};
Render.prototype.nostroke = function () {
	this.renderactions.push(new RenderAction('nostroke'));
	return this;
};
Render.prototype.color = function (color) {
	this.renderactions.push(new RenderAction('color',color));
	return this;
};
Render.prototype.rect = function (detail) {
	this.renderactions.push(new RenderAction('rect',detail));
	return this;
};
Render.prototype.circle = function (detail) {
	this.renderactions.push(new RenderAction('circle',detail));
	return this;
};
Render.prototype.roundrect = function (detail) {
	this.renderactions.push(new RenderAction('roundrect',detail));
	return this;
};
Render.prototype.roundsquare = function (detail) {
	this.renderactions.push(new RenderAction('roundsquare',detail));
	return this;
};
Render.prototype.square = function (detail) {
	this.renderactions.push(new RenderAction('square',detail));
	return this;
};
Render.prototype.text = function(text) {
	this.renderactions.push(new RenderAction('text',text));
	return this;
};
Render.prototype.img = function(img) {
	this.renderactions.push(new RenderAction('img',img));
	return this;
};
Render.prototype.line = function(line) {
	this.renderactions.push(new RenderAction('line',line));
	return this;
};
Render.prototype.path = function(path) {
	this.renderactions.push(new RenderAction('path',path));
	return this;
};
function State() {
	this.state = 0;
	return this;
};
State.prototype.active = function() {
	this.state = 1;
};
State.prototype.inactive = function() {
	this.state = 0;
};
State.prototype.is_inactive = function() {
	return (this.state == 0);
};
State.prototype.is_active = function() {
	return (this.state == 1);
};
State.prototype.mimic = function(meme) {
	this.state = meme.state;
};
function RenderState(oState) {
	if(oState) {
		this.stroke = oState.stroke;
		this.fill = oState.fill;
		this.color = oState.color;
		this.image = oState.image;
	}
	else {
		this.stroke = new State();
		this.fill = new State();
		this.color = '';
		this.image = new State();
	}
	return this;
};
RenderState.prototype.mimic = function(meme) {
	var that = this;
	(function (newstroke,newfill,newcolor,newimage) {
		that.stroke.mimic(newstroke);
		that.fill.mimic(newfill);
		that.color = newcolor;
		that.image.mimic(newimage);
	})(meme.stroke,meme.fill,meme.color,meme.image);
};
RenderState.prototype.is_blank = function() {
	return (this.stroke.is_inactive() && this.fill.is_inactive() && this.color == '' && this.image.is_inactive());	
};
function RenderAction(name,detail) {
	this.name = name;
	this.detail = detail;
	return this;
};
Render.prototype.renderTo = function(srcElement, context,oncomplete) {
	var state = new RenderState();
	var final_state = state;
	var renderq = [];
	var stateq = [];
	for(var ra in this.renderactions) {
		var action = this.renderactions[ra];
		switch(action.name) {
			case 'fill':
				state.fill.active();
				break;
			case 'stroke':
				state.stroke.active();
			break;
			case 'nofill':
				state.fill.inactive();
				break;
			case 'nostroke':
				state.stroke.inactive();
				break;
			case 'color':
				state.color = action.detail;
				if(!state.is_blank()) {
					final_state = state;
					if(stateq.length > 0) {
						while(stateq.length > 0) {
							var blankstate = stateq.pop();
							blankstate.mimic(state);
							state = blankstate;
						}
						state = new RenderState();
					}
				}
				break;
			case 'img':
				state.image.active();
				renderq.push([action,new RenderState(state)]);
				if(!state.is_blank())
					final_state = state;
				state = new RenderState();
				break;
			case 'rect':
				var rstate = new RenderState(state);
				renderq.push([action,rstate]);
				if(!state.is_blank()) {
					final_state = state;
					while(stateq.length > 0) {
						var blankstate = stateq.pop();
						blankstate.mimic(state);
					}
				}
				else
					stateq.push(rstate);
				state = new RenderState();
				break;
			case 'circle':
				var rstate = new RenderState(state);
				renderq.push([action,rstate]);
				if(!state.is_blank()) {
					final_state = state;
					while(stateq.length > 0) {
						var blankstate = stateq.pop();
						blankstate.mimic(state);
					}
				}
				else
					stateq.push(rstate);
				state = new RenderState();
				break;
			case 'roundrect':
				var rstate = new RenderState(state);
				renderq.push([action,rstate]);
				if(!state.is_blank()) {
					final_state = state;
					while(stateq.length > 0) {
						var blankstate = stateq.pop();
						blankstate.mimic(state);
					}
				}
				else
					stateq.push(rstate);
				state = new RenderState();
				break;
			case 'roundsquare':
				var rstate = new RenderState(state);
				renderq.push([action,rstate]);
				if(!state.is_blank()) {
					final_state = state;
					while(stateq.length > 0) {
						var blankstate = stateq.pop();
						blankstate.mimic(state);
					}
				}
				else
					stateq.push(rstate);
				state = new RenderState();
				break;
			case 'square':
				var rstate = new RenderState(state);
				renderq.push([action,rstate]);
				if(!state.is_blank()) {
					final_state = state;
					while(stateq.length > 0) {
						var blankstate = stateq.pop();
						blankstate.mimic(state);
					}
				}
				else
					stateq.push(rstate);
				state = new RenderState();
				break;
			case 'text':
				var rstate = new RenderState(state);
				renderq.push([action,rstate]);
				if(!state.is_blank()) {
					final_state = state;
					while(stateq.length > 0) {
						var blankstate = stateq.pop();
						blankstate.mimic(state);
					}
				}
				else
					stateq.push(rstate);
				state = new RenderState();
				break;
			case 'line':
				var rstate = new RenderState(state);
				renderq.push([action,rstate]);
				if(!state.is_blank()) {
					final_state = state;
					while(stateq.length > 0) {
						var blankstate = stateq.pop();
						blankstate.mimic(state);
					}
				}
				else
					stateq.push(rstate);
				state = new RenderState();
				break;
			case 'path':
				renderq.push([action,new RenderState(state)]);
				if(!state.is_blank())
					final_state = state;
				state = new RenderState();
				break;
			default:
				break;	
		}
	}
	var that = this;
	for (var qi in renderq) {
		var rendercall;
		var actionstate = renderq[qi];
		var action = actionstate[0];
		var wstate = actionstate[1];
		if (wstate.is_blank() && !final_state.is_blank()) {
			wstate = final_state;		
		}
		switch(action.name) {
			case 'rect':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						that.provider.drawRect(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'circle':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						that.provider.drawCircle(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;			
			case 'roundrect':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						that.provider.drawRoundrect(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'roundsquare':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						that.provider.drawRoundSquare(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'square':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						that.provider.drawSquare(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'text':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						that.provider.drawText(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'img':
				var loadhook = function () { 
					srcElement.imageloading -= 1;
					while(srcElement.waitq.length > 0) {
						var q = srcElement.waitq.pop();
						if(q.wstate.image.is_active()) {
							srcElement.imageloading += 1;
							q.rendercall();
							return;
						}
						q.rendercall();
					}
					
					if(srcElement.imageloading == 0) 
						oncomplete();
				};
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail,loadhooki) {
					return function () {
						that.provider.drawImage(srcElementc,contextc,wstatec,actioncdetail,loadhooki);
					};
				})(srcElement,context,wstate,action.detail,loadhook);
				break;
			case 'line':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						that.provider.drawLine(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'path':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						that.provider.drawPath(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			default:
				break;
		}
		if (srcElement.imageloading > 0) {
			srcElement.waitq.push({ 'wstate': wstate, 'rendercall' : rendercall});
			continue;
		}
		else if (wstate.image.is_active()) {
			srcElement.imageloading += 1;
		}
		rendercall();			 
	}
	if(srcElement.imageloading == 0) {
		while(srcElement.waitq.length > 0) {
			var q = srcElement.waitq.pop();
			if(q.state.image.is_active()) {
				srcElement.imageloading += 1;
				q.rendercall();
				return;
			}
			q.rendercall();
		}
		srcElement.launchable = 1;
		if(srcElement.imageloading == 0)
			oncomplete();
	}
	
};
