// Render Provider is the actual meat of interacting with canvas.
// It provides the API's lowest level rendering functionality.

function RenderProvider() {
	return this;
};
RenderProvider.prototype.drawImage = function(srcElement,context,state,detail,loadhook) {
	if(!detail)
		return;
	var width = srcElement.loc.abs.xsz;
	var height = srcElement.loc.abs.ysz;
	switch(detail.constructor) {
		case String:
			var img = new Image();
			img.onload = function() {
				context.drawImage(img,0,0,width,height);
				loadhook();
			};
			img.src = detail;
			break;
		case HTMLImageElement:
			if(detail.complete) {
				context.drawImage(detail,0,0,width,height);
				loadhook();
			}
			else {
				
				detail.onload = function() {
					context.drawImage(detail,0,0,width,height);
					loadhook();	
				};
			}
			break;
		default:
			break;
	}
};
RenderProvider.prototype.parseTextDetail = function(srcElement,context,detail) {
	var xdefault = 0.5;
	var ydefault = 0.5;
	var sizedefault = 0.1;
        var raw = detail.trim().replace('!@', '^^^REPLACEDAT^^^');
	var numberStart = /^@ *\d+\.\d+/;
	var atAt = /@/;
	var atSpace = /\s+/;
	var atComma = /,/;
	var width = srcElement.loc.abs.xsz;
	var height = srcElement.loc.abs.ysz;
	var md = width < height ? width : height; 
	var font = 'px Helvetica'; //our font
        if (raw.match(numberStart)) {
		raw = raw.substring(1).trim();
        	var raws = raw.splitf(atSpace);
		var fontSize = isNumber(raws[0].trim()) ? parseFloat(raws[0].trim()) : sizedefault; // default is 0.1
		var raws2 = raws[1].splitf(atAt);
		var text = raws2[0].trim();
		if(text.length == 0)
			return;	
		var xpos = xdefault;
		var ypos = ydefault;
		if(raws2.length > 1) {
			var raws3 = raws2[1].splitf(atComma);
			xpos = isNumber(raws3[0].trim()) ? parseFloat(raws3[0].trim()) : xpos;
			if(raws3.length > 1) 
				ypos = isNumber(raws3[1].trim()) ? parseFloat(raws3[1].trim()) : ypos; 
		}	
	}
	else {
		var raws = raw.splitf(atAt);
		var text = raws[0].trim();
		if(text.length == 0)
			return;		
		var ypos = ydefault;
		if(raws.length > 1) {
			var raws2 = raws[1].trim().splitf(atSpace);
			if(raws2[0].charAt(raws2[0].length-1) == ',') {
				var xpos = isNumber(raws2[0].trim()) ? parseFloat(raws2[0].trim()) : ydefault; 
			}
			else {
				var fontSize = isNumber(raws2[0].trim()) ? parseFloat(raws2[0].trim()) : sizedefault;
			}
			if(raws2.length > 1) {
				var raws3 = raws2[1].trim().splitf(atComma);
				if(raws3.length < 2) {

					if(xpos)
						var ypos = isNumber(raws3[0].trim()) ? parseFloat(raws3[0].trim()) : ydefault;
					else
						var xpos = isNumber(raws3[0].trim()) ? parseFloat(raws3[0].trim()) : xdefault;					
				}	
				else {
					if(xpos)
						var fontSize = xpos;
					else
						var xpos;
					xpos = isNumber(raws3[0].trim()) ? parseFloat(raws3[0].trim()) : xdefault; 
					ypos = isNumber(raws3[1].trim()) ? parseFloat(raws3[1].trim()) : ydefault;	
				}	
			}
			else {
				var raws3 = raws2[0].trim().split(atComma);
				if(raws3.length > 1) {
					if(xpos)
						var fontSize = xpos;
					else
						var xpos;
					xpos = isNumber(raws3[0].trim()) ? parseFloat(raws3[0].trim()) : xdefault;
					ypos = isNumber(raws3[1].trim()) ? parseFloat(raws3[1].trim()) : ydefault;	
				}	
				else {
					if(xpos)
						var fontSize = xpos;
					else
						var xpos = xdefault;	
				}
			}
		}
		else {
			var xpos = xdefault;
			var fontSize = sizedefault;
		}
	}
	font = String(fontSize*md) + font;
	text = text.replace('^^^REPLACEDAT^^^', '@');
	xpos = width * xpos;
	ypos = height * ypos;
	return { 'font' : font, 'text' : text, 'x': xpos, 'y': ypos };
};
RenderProvider.prototype.drawText = function(srcElement,context,state,detail) {
	if(detail == undefined || detail == null || detail.length == 0)
		return;
	if(!state.color || state.color.length == 0) 
		state.color = ENV.color.default();
	var details = this.parseTextDetail(srcElement,context,detail);
	context.fillStyle = state.color;
	context.textAlign = 'center';
	context.textBaseline = 'ideographic';//i just like the word
	context.font = details.font;
	context.fillText(details.text,details.x,details.y);
};
RenderProvider.prototype.parseDetail = function(detailString) {
	var details = [];
	var detailRaw = detailString.split(',');
	for(var dti in detailRaw) 
		details.push(parseFloat(detailRaw[dti].trim()));
	return details;
};
RenderProvider.prototype.drawRect = function(srcElement,context,state,detail) {
	//alert('drawRect called');
	var width = srcElement.loc.abs.xsz;
	var w = width;
	var height = srcElement.loc.abs.ysz;
	var h = height;
	var x = 0;
	var y = 0;
	if(detail != null && detail != undefined && detail.length > 0) {
		var dx,dy,dw,dh;
		var da = this.parseDetail(detail);
		dx = da[0];
		dy = da[1];
		dw = da[2];
		dh = da[3];
		x = dx;
		w = dw*width;
		y = dy;
		h = dh*height;
	}
	x = x * width;
	y = y * height;
	if(!state.color || state.color.length == 0) {
		state.color = ENV.color.default()
	}
	if(state.fill.is_active()) {
		context.fillStyle = state.color;
		context.fillRect(x,y,w,h);
	}
	if(state.stroke.is_active()) {
		context.strokeStyle = state.color;
		context.strokeRect(x,y,w,h);
	}
	if(! state.fill.is_active() && ! state.stroke.is_active()) {
		context.fillStyle = state.color;
		context.fillRect(x,y,w,h);
	}		
	//alert('context w,h = ' + context.canvas.width + ',' + context.canvas.height );
	//alert('x,y,w,h = ' + x + ',' + y + ',' + w + ',' + h );
		
};
RenderProvider.prototype.drawRoundrect = function(srcElement,context,state,detail) {
	var width = srcElement.loc.abs.xsz;

	var w = width;
	var height = srcElement.loc.abs.ysz;
	var h = height;
	var x = 0;
	var y = 0;
	if(detail != null && detail != undefined && detail.length > 0) {
		var dx,dy,dw,dh;
		var da = this.parseDetail(detail);
		dx = da[0];
		dy = da[1];
		dw = da[2];
		dh = da[3];
		x = dx;
		w = dw*width;
		y = dy;
		h = dh*height;
	        var dM = w > h ? w : h;//defaults to 1/16th of maximum dimension
		var dr = dM*1.0/16;  
		if (da.length > 4) // or the 5 dim in the detail if available
			dr = dM*da[4];
	}
	else {
	        var dM = w > h ? w : h;//defaults to 1/16th of maximum dimension
		var dr = dM*1.0/16;  
	}
	x = x * width;
	y = y * height;
	var reduce = Math.ceil(Math.sqrt(2.0)*dr)
	if (reduce > w/2) {
		reduce = w/2;
	}
	if (reduce > h/2) {
		reduce = h/2;
	}
	var cornerNNW = { 'x' : x + reduce, 'y' : y };
	var cornerNWW = { 'x' : x, 'y' : y + reduce };
	var cornerNNE = { 'x' : x + w - reduce, 'y' : y };
	var cornerNEE = { 'x' : x + w, 'y' : y + reduce };
	var cornerSWW = { 'x' : x, 'y' : y + h - reduce };
	var cornerSSW = { 'x' : x + reduce, 'y' : y + h };
	var cornerSEE = { 'x' : x + w, 'y' : y + h - reduce };
	var cornerSSE = { 'x' : x + w - reduce, 'y' : y + h };
	var arcAng = Math.PI/2.0;
	var arcNW = { 'x' : x + reduce, 'y': y + reduce, 'angle' : 2*arcAng };
	var arcNE = { 'x' : x + w - reduce, 'y': y + reduce, 'angle' : 3*arcAng };
	var arcSW = { 'x' : x + reduce, 'y': y + h - reduce, 'angle' : arcAng };
	var arcSE = { 'x' : x + w - reduce, 'y': y + h - reduce, 'angle' : 0 };
	context.beginPath();
     	context.moveTo(cornerNNW.x,cornerNNW.y);
	context.lineTo(cornerNNE.x,cornerNNE.y);
	context.arc(arcNE.x,arcNE.y,reduce,arcNE.angle,arcNE.angle+arcAng);
	context.lineTo(cornerSEE.x,cornerSEE.y);
	context.arc(arcSE.x,arcSE.y,reduce,arcSE.angle,arcSE.angle+arcAng);
	context.lineTo(cornerSSW.x,cornerSSW.y);
	context.arc(arcSW.x,arcSW.y,reduce,arcSW.angle,arcSW.angle+arcAng);
	context.lineTo(cornerNWW.x,cornerNWW.y);
	context.arc(arcNW.x,arcNW.y,reduce,arcNW.angle,arcNW.angle+arcAng);
	if(!state.color || state.color.length == 0) {
		state.color = ENV.color.default()
	}
	if(state.fill.is_active()) {
		context.fillStyle = state.color;
		context.fill();
	}
	if(state.stroke.is_active()) {
		context.strokeStyle = state.color;
		context.stroke();
	}
	if(! state.fill.is_active() && ! state.stroke.is_active()) {
		context.fillStyle = state.color;
		context.fill();
	}
};
RenderProvider.prototype.drawCircle = function(srcElement,context,state,detail) {
	var x = 0;
	var y = 0;
	var radius = 0.2;
	var d = this.parseDetail(detail);
	if(d && d.length > 0) {
		x = d[0] * srcElement.loc.abs.xsz;
		y = d[1] * srcElement.loc.abs.ysz;
		var Md = srcElement.loc.abs.xsz < srcElement.loc.abs.ysz ? srcElement.loc.abs.ysz : srcElement.loc.abs.xsz;
		radius = d[2] * Md;
	}
	context.beginPath();
	context.arc(x,y,radius,0,Math.PI*2);
	if(!state.color || state.color.length == 0) {
		state.color = ENV.color.default()
	}
	if(state.fill.is_active()) {
		context.fillStyle = state.color;
		context.fill();
	}
	if(state.stroke.is_active()) {
		context.strokeStyle = state.color;
		context.stroke();
	}
	if(! state.fill.is_active() && ! state.stroke.is_active()) {
		context.fillStyle = state.color;
		context.fill();
	}
};
RenderProvider.prototype.drawRoundSquare = function(srcElement,context,state,detail) {
	var d = this.parseDetail(detail);
	var xyRatio = 1.0*srcElement.loc.abs.xsz/srcElement.loc.abs.ysz;	
	d.splice(3,0,xyRatio*d[2]);
	args = d.join(',');
	//alert(args);
	return this.drawRoundrect(srcElement,context,state,args);	
};
RenderProvider.prototype.drawSquare = function(srcElement,context,state,detail) {
	var d = this.parseDetail(detail);
	var xyRatio = 1.0*srcElement.loc.abs.xsz/srcElement.loc.abs.ysz;	
	d.push(xyRatio*d[2]);
	args = d.join(',');
	return this.drawRect(srcElement,context,state,args);	
};
RenderProvider.prototype.drawLine = function(srcElement,context,state,detail) {
	var width = srcElement.loc.abs.xsz;
	var height = srcElement.loc.abs.ysz;
	var lw = 0.02;
	var x1 = 0;
	var y1 = 0;
	var x2 = width;
	var y2 = 0;
	if(detail)
		var d = this.parseDetail(detail);
	if(d && d.length > 0) {
		lw = d[0] || lw;
		x1 = d[1] || x1;
		y1 = d[2] || y1;
		x2 = d[3] || x2;
		y2 = d[2] || y2;
		if(d.length > 4)		
			y2 = d[4];
	}
	var md = width < height ? width : height;
	x1 *= width;
	x2 *= width;
	y1 *= height;
	y2 *= height;
	lw *= md;
	context.beginPath();
	context.moveTo(x1,y1);
	context.lineTo(x2,y2);
	if(!state.color || state.color.length == 0) {
		state.color = ENV.color.default()
	}
	context.strokeStyle = state.color;
	context.lineWidth = lw;
	context.stroke();
};
RenderProvider.prototype.drawPath = function(srcElement,context,state,detail) {

};
