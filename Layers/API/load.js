var APILaunchAppInContext = function (lastToLoad) {
	var basepath = '/Layers/API/';
	var attachbody = document.getElementsByTagName('body')[0];
	function loadAPIFramework () {
		scripts = [
			{ path : 'First/' , scripts : [ 
						'A1First' 
				] },
			{ path : 'Visual/', scripts : [ 
						'ColorSource',
						'CanvasServer',
						'RenderProvider'
				] },
			{ path : 'Structural/' , scripts : [
						'ENV',
						'Extensions',
						'Locate'
				] },
			{ path : 'Visual/' , scripts : [
						'Render'
				] },
			{ path : 'Structural/' , scripts : [
						'Element',
						'Window'					
				] }
		];
		function loadSeq () {
			seq = [];
			for(pathRoute in scripts) {
				var path = scripts[pathRoute].path;
				var pathscripts = scripts[pathRoute].scripts;
				for (i in pathscripts) {
					var scriptPath = path + pathscripts[i] + '.js';
					seq.push(scriptPath);
				}
			}
			return seq;
		};
		loadseq = loadSeq();
		function loadScript (iterator,pathRelativeToLoad,nobase) {
			var scripttag = document.createElement('script');
			if(!nobase) {
				function loadnext() {
					//alert('On load of ' + iterator + ' attempt to load ' + (iterator+1));
					var script = loadseq[iterator+1];
					if(script)
						loadScript(iterator+1,script);
					else 
						loadScript(null,lastToLoad,1);
				
				};
				if( scripttag.addEventListener ) {
					scripttag.addEventListener('load',loadnext);
				} else if ( scripttag.attachEvent ) {
					scripttag.attachEvent('onload',loadnext);
				}
			};
			scripttag.src = !nobase ? basepath + pathRelativeToLoad : pathRelativeToLoad;
			attachbody.appendChild(scripttag);
		};
		function loadFirst() {
			var script0 = loadseq[0];
			loadScript(0,script0);
		};
		loadFirst();
	};
	if(window.addEventListener) {
		window.addEventListener('load',loadAPIFramework);	
	} else if (window.attachEvent) {
		window.attachEvent('onload',loadAPIFramework);	
	}
	
};
