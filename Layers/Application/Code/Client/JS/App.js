// Contains the Application Code based on this API
// That at the moment is for testing and developing the API
// By design we run it inside the window.onload event.
// We may later provide for adding the application's code into window onload by way of an API function.

var appMain = function () {
	var basepath = '/Layers/Application/';
	var bg = new Free().
			renders(
				_().
				color(Color.black()).
				line('0.4,0,0.2,1,0.2').
				circle('0.5,0.5,0.4').
				color(Color.blue(0.5)).
				rect()
				).
			draw();
	var katie = new Free().
			relocate(0.2,0.2,0.6,0.6).
			renders(
				_().
				img(basepath+'Media/Images/t.jpg')
				).
			draw();
	function resizeElems () {
		bg.clear();
		bg.onscreenContext.canvas.width = window.innerWidth;
		bg.onscreenContext.canvas.height = window.innerHeight;
		bg.at(bg.onscreenContext);//goLive(requestDistinctOnscreenContext()));
		bg.draw();
		katie.clear();
		katie.onscreenContext.canvas.width = window.innerWidth;
		katie.onscreenContext.canvas.height = window.innerHeight;
		katie.at(katie.onscreenContext);//goLive(requestDistinctOnscreenContext()));
		katie.draw();
	};
	if (window.addEventListener) {
		window.addEventListener('resize',resizeElems);
	} else if (window.attachEvent) {
		window.addEventListener('onresize',resizeElems);
	}

};
appMain();
