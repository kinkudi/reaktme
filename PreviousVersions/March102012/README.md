# Reakt.me powers the social web
# Currently under active development.
# Here is the current IDEAS and TODO

//ToDo Summary
//1.Do views for apps
//2.Fix / improve API based on discovery of doing views
//3. Add events to API.
//4. Build interaction pages of apps
//5. Fix/improve API based on discovery of doing interaction pages
//6. Build Flow / UI automaton / state transitions
//7. Build concise flows for apps
//8. Fix/improve API based on discovery of doing flows
//9. Add animation support to API
//10. Build animation into flows, UI.
//11. Fix API based on discovery of building in animation.
//12. Incorporate Paths / Icons / Scalable graphics into API.
//13. Build these into apps.
//14. Fix API based on discovery of building these sprites into apps.

//Ideas Summary
//1. Wrapper for Canvas. 
//2. Every Canvas (and every Element) has a unique URI to which we can get and post.
//3. Text should have a way to specify a size as a proportion of screen width and handle multiline seamlessly.
//4. Function chain to allow Color API like _().color(green).rect() or  _().color().green().rect()
