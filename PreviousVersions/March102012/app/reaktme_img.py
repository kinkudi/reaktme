import webapp2
from models.RImage import add

class Collector(webapp2.RequestHandler):
	def post(self):
		self.response.headers['access-control-allow-origin'] = '*'
		self.response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
		img = self.request.POST.get('img')
		w = self.request.POST.get('w')
		h = self.request.POST.get('h')
		key,url = add(1001,img,w,h)
		self.response.out.write("Image Saved " + key + " at " + url)

	def get(self):
		self.redirect('http://reakt.me/')

app = webapp2.WSGIApplication([
	(r'/reaktmeImageUpload/.*',Collector)
	])

def main():
	app.run()

if __name__ == "__main__":
	main()


