from google.appengine.ext import db

#For logging in

class LoginKey(db.Model): #indexed by user login name 
	usernumber=db.IntegerProperty(indexed=False) #user number
	loginsecret=db.StringProperty(indexed=False) #user password
	secret_reset_address=db.EmailProperty(indexed=False) #reset secret
	
	account_status=db.IntegerProperty(indexed=False) #active, suspended, locked, terminated
	login_history=db.ListProperty(db.Key,indexed=False) #login instances 

class LoginInstance(db.Model): #u can create an account by trying to login
	name_attempt=db.StringProperty(indexed=False)
	secret_attempt=db.StringProperty(indexed=False)
	when_attempted=db.DateTimeProperty(auto_now_add=True,indexed=False)
	who_attempted=db.IntegerProperty(indexed=True) #oh i can make it integer
	user_agent=db.StringProperty(indexed=False)
	country=db.StringProperty(indexed=False)
	success_status=db.BooleanProperty(indexed=False)
	account_created_status=db.IntegerProperty(indexed=False) #represents progress status of an attempt to create an account	

def login_attempt(name,secret,ip,uagent,country):#create a login instance and return it
	li = LoginInstance()
	li.name_attempt = name
	li.secret_attempt = secret
	li.who_attempted = integer_ip(ip)
	li.user_agent = uagent
	li.country = country
	li.success_status = validate(name,secret)
	li.put()
	if not li.success_status:
		nl = LoginKey(key_name=name)
		nl.usernumber = 1001
		nl.loginsecret = secret
		nl.account_status = 1
		nl.secret_reset_address = db.Email('crisfolio@gmail.com')
		nl.login_history = [li.key()]
		nl.put()
	return li

def validate(name,secret): #more complex behaviour to optimise requests flow and account creation at db transcation flow level to come
	login_key = LoginKey.get_by_key_name(name)
	if login_key == None:
		return False
	if login_key.loginsecret != secret:
		return False
	return True

def integer_ip(string_ip):
	codes = string_ip.strip().split('.')
	mult = (1 << 24)
	iip = 0
	for code in codes:
		code_int = int(code.strip())
		iip += (code_int * mult)
		mult >>= 8
		if mult == 0:
			mult = 1
	return iip

