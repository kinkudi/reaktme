var draggables = [null];
var draggableFlow = new Flow(0);
var draggableLock = 1;
if(crdrag == undefined)
	var crdrag = 0;
draggableFlow.registerLaunchAction( new Action (
  function () {
     d = draggables[0];
     mouseDown = 0;
     channels.registerLock('exclusiveDrag');
     draggableFlow.registerPanel(d);
  }
));
function dragHandler(e) {
    if(mouseDown == 0 || crdrag > 0)
        return;
    var mx = e.clientX;
    var my = e.clientY;
    if(d.inside(mx,my)) {
        d.respondDrag(mx,my);
    }
};
function registerDown(e) {
    var ox = e.clientX;
    var oy = e.clientY;
    if(d.inside(ox,oy) && channels.test('exclusiveDrag')<draggableLock) {
        channels.lock('exclusiveDrag',draggableLock);
        mouseDown = 1;
        d.rememberDragPosition(ox,oy);
    }
};
function registerUp(e) {
    if(mouseDown==0)
        return;
    mouseDown = 0;
    channels.unlock('exclusiveDrag');
   };
draggableFlow.registerListener(document,'mousedown',registerDown);
draggableFlow.registerListener(document,'mouseup',registerUp);
draggableFlow.registerListener(document,'mousemove',dragHandler );

