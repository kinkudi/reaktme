var imageDataURLs = {};
function fileSelectHandler(e) {
    files = e.target.files;
    var len = files.length;
    for(var filei = 0; filei < len; filei += 1) {
            var aFile = files[filei];
	    var fileReader = new FileReader();
	    fileReader.onload = (function (theFile) {
		return function(e) {
			if(e.target.result != null && e.target.result != undefined) {
			    var imge = new Image();
			    imge.src = e.target.result;
			    imageDataURLs[theFile.name] = imge;
                            imge.onload = (function () {
                                return function(e) {
			            draggables[0] = new Draggable(g3,imge);
                                    draggableFlow.launch();
                                 };
                            })();
			}
		 };
	    })(aFile);
            fileReader.readAsDataURL(aFile);
    }    
};
var fileSelectFlow = new Flow(0);
fileSelectFlow.registerLaunchAction ( new Action ( 
    function () {
    	ixj.click();
    }
));
fileSelectFlow.registerListener(ixj,'change', fileSelectHandler );

