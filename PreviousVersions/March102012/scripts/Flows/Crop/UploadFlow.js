var base16 = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'];
var sendImageToServer = function(imgDataObject,imgDataURL) {
	var iwidth = imgDataObject.width;
	var iheight = imgDataObject.height;
	var xhr = new XMLHttpRequest();
	imgDataURL = imgDataURL.replace(/^data:image\/(png|jpg);base64,/, "");
	xhr.onreadystatechange = function () {
		if(xhr.readyState == 4 && xhr.status == 200) {
			alert("Callback:"+xhr.responseText);
		}
	};
	xhr.open('POST','/reaktmeImageUpload/',true);
	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	xhr.send('&img='+encodeURIComponent(imgDataURL)+'&w='+iwidth+'&h='+iheight);
};
var uploadFlow = new Flow(1);
uploadFlow.registerLaunchAction ( new Action (
	function () {
		var imgData = null;
		var imgDataURL = null;
		if(draggables && (draggables[0] == null)) {
			fileSelectFlow.launch();
			return;
		}
		if(cropcon.cropFlowLaunched) {
			var sx = cr.x;
			var sy = cr.y;
			var w = cr.w;
			var h = cr.h;
			var drawCanvas = draggables[0].p;
			imgData = drawCanvas.getImageData(sx,sy,w,h);
			c4.width = w;
			c4.height = h;
			g4.putImageData(imgData,0,0);
			imgDataURL = c4.toDataURL();
		}
		else {
			var img = draggables[0];
			var sx = img.x;
			var sy = img.y;
			var w = img.w;
			var h = img.h;
			var drawCanvas = draggables[0].p;
			imgData = drawCanvas.getImageData(sx,sy,w,h);
			c4.width = w;
			c4.height = h;
			g4.putImageData(imgData,0,0);
			imgDataURL = c4.toDataURL();
		}
		if(imgData != null) {
			sendImageToServer(imgData,imgDataURL);
		}
	}
));

