function LoginPanel () {
    var args = Array.prototype.slice.call(arguments);
    this.parent.constructor.apply(this,args);
    this.fcolor = "hsla(40,100%,70%,1.0)";
    this.ecolor = "hsla(40,100%,50%,1.0)";
    this.linea = 0.2;
    this.respondResize();
    this.field = new TextField(this.g,this.x+this.w*0.17,this.y+this.h*0.23,this.w*0.66,this.h*0.2);
    this.field2 = new TextField(this.g,this.x+this.w*0.17,this.y+this.h*0.56,this.w*0.66,this.h*0.2,1);
    this.field.succesor = this.field2;
    this.field2.succesor = this.field;
    this.field.isLastOnPage = 1;
}
LoginPanel.inheritsFrom( Card );
LoginPanel.prototype.draw = function () {
    this.parent.draw.call(this,arguments);
    this.field.redraw();
    this.field2.redraw();  
};
LoginPanel.prototype.clear = function () {
    this.parent.clear.call(this,arguments);
    this.field.clear();
    this.field2.clear();
};
LoginPanel.prototype.submit = function () {
	var thist = this;
	return function () {
		if(thist.field.value.length > 0 && thist.field2.value.length > 0) {
			var name = encodeURIComponent(thist.field.value);
			var secret = encodeURIComponent(thist.field2.value);
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function () {
				if(xhr.readyState == 4 && xhr.status == 200) {
					alert("Callback:"+xhr.responseText);
				}
				return;
			};
			xhr.open('POST','/reaktmeDoor/'+name,true);
			xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			xhr.send('&name='+name+'&secret='+secret);
		}
	};
};
LoginPanel.prototype.respondResize = function () {
    this.wa = (g.canvas.width < 200 ? 0.99 : (g.canvas.width > 640 ? 480 : 0.67));
    this.ha = 0.5;
    this.parent.respondResize.call(this,arguments);
    this.prerender();
    if(this.field && this.field2) {
        this.field.respondResize(this.x+this.w*0.15,this.y+this.h*0.23,this.w*0.7,this.h*0.2);    
        this.field2.respondResize(this.x+this.w*0.15,this.y+this.h*0.56,this.w*0.7,this.h*0.2);    
        this.draw();
    }
};

