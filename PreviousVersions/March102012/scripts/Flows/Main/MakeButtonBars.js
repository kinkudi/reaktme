var r = new Renderer();
var bb = new ButtonBar(g);

var selectb = new Button();
selectb.registerAction( new Action (
    function () {
       // run the file select flow
       //alert("Select file");
       fileSelectFlow.launch();
    }

));
selectb.registerRenderer(r.threePhoto);
selectb.prerender();

var cropcon = new Button();
cropcon.registerAction( new Action (
    function () {
	//worry about making this work later
	if(cropcon.cropFlowLaunched == 1) {
		cropFlow.land();
		cropcon.cropFlowLaunched = 0;
		return;
	}
	cropcon.cropFlowLaunched = 1;
	cropFlow.launch();
    }
));
cropcon.registerRenderer(r.cropcon);
cropcon.prerender();

var uploadb = new Button();
uploadb.registerAction( new Action (
     function () {
	 // run the upload flow, which consists of
         // check if our image data exists
         // if so send to server via XMLHTTP
         // if not select the current region and prompt the user (in visuals not language)
         // "Is this okay? Press to confirm, or click crop to change"
	uploadFlow.launch();  
	}
));
uploadb.registerRenderer(r.stick);
uploadb.registerRenderer(r.camera,0.1,-0.2);
uploadb.prerender();


var everyone = new Button();
everyone.registerRenderer(r.threestick);
everyone.prerender();
everyone.registerAction( new Action ( 
    function () {
        alert("Hey this is everyone");
    }
));

var onewayhi = new Button();
onewayhi.registerRenderer(r.lstick,0.05);
onewayhi.registerRenderer(r.stick,0.2);
onewayhi.prerender();

var mutualhi = new Button(g,500,100);
mutualhi.registerRenderer(r.lstick,0.05);
mutualhi.registerRenderer(r.rstick,-0.05);
mutualhi.prerender();

var arrangemeet = new Button(g,100,300);
arrangemeet.registerRenderer(r.lstick);
arrangemeet.registerRenderer(r.rstick);
arrangemeet.registerRenderer(r.clock);
arrangemeet.prerender();

var gomeet = new Button();
gomeet.registerRenderer(r.lstick,0.1);
gomeet.registerRenderer(r.rstick,-0.1);
gomeet.prerender();

var info = new Button(g,0.1,0.7);
info.registerRenderer(r.i);
info.prerender();

var login = new Button(g,0.1,0.7);
login.registerAction( new Action (
	function () {
		if( loginFlow.loginFlowLaunched == 1 ) {
			loginFlow.land();
			loginFlow.loginFlowLaunched = 0;
			return;
		}
		loginFlow.loginFlowLaunched = 1;
		loginFlow.launch();
	}
));
login.registerRenderer(r.i);
login.prerender();

bb.addButton(login);
//bb.addButton(info);
//bb.addButton(everyone);
//bb.addButton(onewayhi);
//bb.addButton(mutualhi);
//bb.addButton(arrangemeet);
//bb.addButton(gomeet);
//bb.addButton(selectb);
//bb.addButton(cropcon);
//bb.addButton(uploadb);

bb.respondResize();
bb.draw();

