var r = new Renderer();
var bc = new ButtonCard(g);

name_field = new ButtonTicker(g);
name_field.preRender();
bc.addOutputField(name_field,'name');

secret_field = new ButtonTicker(g);
secret_field.left = 1;
secret_field.preRender();
bc.addOutputField(secret_field,'secret');

var submit = bc.submit();

var secret_submit_action = function () {
	if(name_field.value.length > 0)
		submit();
	secret_field.blur();
	name_field.focus();
};

var name_submit_action = function () {
	//if(secret_field.value.length > 0) 
	//	submit();
        name_field.blur();
	secret_field.focus();
};

var name_input = name_field.addSymbol(name_submit_action);
var secret_input = secret_field.addSymbol(secret_submit_action);

var addSymbol = function (btn) {
	if(name_field.hasFocus != 0)
		name_input(btn);
	else if(secret_field.hasFocus != 0)
		secret_input(btn);
};

name_field.focus();


var b0 = new Button();
b0.registerAction( new Action (
    function () {
       //alert("0 pressed");
       addSymbol(b0);
    }
));
b0.registerTextRenderer(r,'0');
b0.prerender();

var b1 = new Button();
b1.registerAction( new Action (
    function () {
       //alert("1 pressed");
       addSymbol(b1);
    }

));
b1.registerTextRenderer(r,'1');
b1.prerender();

var b2 = new Button();
b2.registerAction( new Action (
    function () {
       //alert("2 pressed");
       addSymbol(b2);
 }

));
b2.registerTextRenderer(r,'2');
b2.prerender();


var b3 = new Button();
b3.registerAction( new Action (
    function () {
       //alert("3 pressed");
       addSymbol(b3);      
    }

));
b3.registerTextRenderer(r,'3');
b3.prerender();


var b4 = new Button();
b4.registerAction( new Action (
    function () {
       //alert("4 pressed");
       addSymbol(b4);  
    }
));
b4.registerTextRenderer(r,'4');
b4.prerender();


var b5 = new Button();
b5.registerAction( new Action (
    function () {
       //alert("5 pressed");
       addSymbol(b5); 
    }

));
b5.registerTextRenderer(r,'5');
b5.prerender();


var b6 = new Button();
b6.registerAction( new Action (
    function () {
       //alert("6 pressed");
       addSymbol(b6);    
    }
));
b6.registerTextRenderer(r,'6');
b6.prerender();


var b7 = new Button();
b7.registerAction( new Action (
    function () {
       //alert("7 pressed");
       addSymbol(b7);
    }

));
b7.registerTextRenderer(r,'7');
b7.prerender();


var b8 = new Button();
b8.registerAction( new Action (
    function () {
       //alert("8 pressed");
       addSymbol(b8);
    }
));
b8.registerTextRenderer(r,'8');
b8.prerender();


var b9 = new Button();
b9.registerAction( new Action (
    function () {
       //alert("9 pressed");
       addSymbol(b9);
    }
));
b9.registerTextRenderer(r,'9');
b9.prerender();


var be = new Button();
be.registerAction( new Action (
    function () {
       //alert("= pressed");
       addSymbol(be);
    }
));
be.registerTextRenderer(r,'=');
be.prerender();


var ba = new Button();
ba.registerAction( new Action (
    function () {
       //alert('+ pressed');
        addSymbol(ba);
    }
))
ba.registerTextRenderer(r,'+');
ba.prerender();


var bm = new Button();
bm.registerAction( new Action (
    function () {
       addSymbol(bm);
    }
));
bm.registerTextRenderer(r,'*');
bm.prerender();


var bsmile = new Button();
bsmile.text = 'butt';
bsmile.registerAction( new Action (
    function () {
       //alert("Smile pressed");
       addSymbol(bsmile);    
    }
));
bsmile.registerRenderer(r.twicehead);
bsmile.prerender();


var bman = new Button();
bman.text = 'man';
bman.registerAction( new Action (
    function () {
       //alert("Man pressed");
       addSymbol(bman);
    }
));
bman.registerRenderer(r.man);
bman.prerender();


var bwoman = new Button();
bwoman.text = 'woman';
bwoman.registerAction( new Action (
    function () {
       addSymbol(bwoman);
    }
));
bwoman.registerRenderer(r.woman);
bwoman.prerender();

bnothing = new Button();
bnothing.text = '';
bnothing.registerRenderer(r.clear);
bnothing.prerender();

bsubmit = new Button();
bsubmit.text = '!'
bsubmit.registerAction( new Action (
    function () {
       //alert("Submit pressed");
       addSymbol(bsubmit);
    }
));
bsubmit.fcolor = "hsla(120,100%,50%,1.0)";

bsubmit.registerRenderer(r.bam);
bsubmit.prerender();

bback = new Button();
bback.text = '<'
bback.registerAction( new Action (
    function () {
       //alert("Back pressed");
       addSymbol(bback);
    }
));
bback.fcolor = "hsla(0,100%,50%,1.0)";
bback.registerRenderer(r.umstick);
bback.prerender();

bc.addButton(b1);
bc.addButton(b2);
bc.addButton(b3);
bc.addButton(b4);
bc.addButton(b5);
bc.addButton(b6);
bc.addButton(b7);
bc.addButton(b8);
bc.addButton(b9);
bc.addButton(be);
bc.addButton(b0);
bc.addButton(bm);
bc.addButton(bman);
bc.addButton(ba);
bc.addButton(bwoman);
bc.addButton(bsubmit);
bc.addButton(bsmile);
bc.addButton(bback);


bc.respondResize();
bc.draw();

