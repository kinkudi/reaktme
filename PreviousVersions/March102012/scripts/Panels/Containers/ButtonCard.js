function ButtonCard (g) {
    this.g = g || document.getElementById('c').getContext('2d');
    this.buttons = [];
    this.outputs = {};
    this.resizeArgs = {};
    this.page = 0;
    this.pages = [];
    this.linea = 0.2;
    this.rowLength = 3.0;
    this.respondResize();
};
ButtonCard.inheritsFrom( Card );
ButtonCard.prototype.submit = function () {
	var thist = this;
	return function () {
                var name_field = thist.outputs['name']
                var secret_field = thist.outputs['secret'] 
		if(name_field.value.length > 0 && secret_field.value.length > 0) {
			var name = encodeURIComponent(name_field.valueString());
			var secret = encodeURIComponent(secret_field.valueString());
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function () {
				if(xhr.readyState == 4 && xhr.status == 200) {
					alert("Callback:"+xhr.responseText);
				}
				return;
			};
			xhr.open('POST','/reaktmeDoor/'+name,true);
			xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			xhr.send('&name='+name+'&secret='+secret);
		}
	};
};

ButtonCard.prototype.getKeyValues = function () {
    this.bs = this.buttons.length;
    if(this.bs == 0)
	return;
    this.rows = Math.ceil(1.0*this.bs/this.rowLength);
    this.h = this.md*0.75;
    this.w = this.h*(this.rowLength/this.rows*1.0);
    this.bw = Math.round(this.w*1.0 /this.rowLength);
    this.x = (this.g.canvas.width - this.w)/2;
    this.y = (this.g.canvas.height - this.h)/2;
};
ButtonCard.prototype.respondResize = function () {
    this.parent.respondResize.call(this,arguments);
    this.md = window.innerWidth < window.innerHeight ? window.innerWidth : window.innerHeight;
    this.getKeyValues();
    for(b in this.buttons) {
        var bt = this.buttons[b];
        bt.wa = this.bw;
        bt.ha = this.bw;
        bt.linea = this.linea;
        bt.respondResize();
    }
    for(o in this.outputs) {
	var op = this.outputs[o];
	op.respondResize(this.x,this.y,this.w);
        op.bw = this.bw;
        if(op.left) 
		op.ex = this.x - this.bw;
        else
		op.ex = this.x + this.w;
    } 
    this.prerender();
    this.draw(); 
};
ButtonCard.prototype.addButton = function (b) {
    this.buttons.push(b);
};
ButtonCard.prototype.addOutputField = function(of,fieldName) {
    this.outputs[fieldName] = of;
};
ButtonCard.prototype.prerender = function () {
    this.pagebuffer = renderToCanvas(this.w,this.h,this);
};
ButtonCard.prototype.clear = function () {
    this.parent.clear.call(this,arguments);
};
ButtonCard.prototype.draw = function () {
    //this.parent.draw.call(this,arguments);
    this.g.clearRect(this.x-2,this.y-2,this.w+4,this.h+4);
    this.g.drawImage(this.pagebuffer,this.x,this.y);
};
ButtonCard.prototype.render = function (gb) { //can maybe use composite
    var limit = this.bs; 
    for(var i = 0; i < limit; i += 1) {
             gb.drawImage(this.buttons[i].buffer,this.bw*(i%this.rowLength),this.bw*Math.floor(i/this.rowLength));  
             this.buttons[i].respondTranslate(this.x+this.bw*(i%this.rowLength),this.y+this.bw*Math.floor(i/this.rowLength));  
    }   
};
ButtonCard.prototype.inside = function (ex,ey) { // could refine this to check x value which shld be upated on every page
    return ( ex >= this.x && ex <= this.x+this.w && ey >= this.y && ey <= this.y+this.h );
};
ButtonCard.prototype.respondPush = function (ex,ey ) {
    // can wrap this in a button action
  	var limit = this.bs;
	for(var i = 0; i < limit; i+= 1) {
          var bb = this.buttons[i];
          if (bb.inside(ex,ey)) {
               bb.execute();
          }    
        }
};

