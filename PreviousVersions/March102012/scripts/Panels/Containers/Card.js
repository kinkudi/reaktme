function Card (g,x,y,w,h,line,corner,ecolor,fcolor) {
    this.g = g || document.getElementById('c').getContext('2d');
    this.xa = x;
    this.ya = y;
    this.wa = w;
    this.ha = h;
    this.linea = line;
    this.corner = corner || "round";
    this.ecolor = ecolor || "hsla(200,80%,80%,1.0)";
    this.fcolor = fcolor || "hsla(200,60%,60%,1.0)";
    this.blur() // for input types
    this.inAnimation = 0;
    this.children = [];
    this.renderers = [];
    this.rendargs = {};
    this.respondResize();
};
Card.prototype.addChild = function (child) {
    this.children.push(child);
};
Card.prototype.blur = function () {
    this.hasFocus = 0;
};
Card.prototype.focus = function () {
   this.hasFocus = 1;
};
Card.prototype.locate = function ( loc, base ) {
    if(loc == 0)
        return 0;
    base = Math.ceil(base);	
    return ((loc > 1.0) ? ((loc < base) ? loc : base - (loc%base)) : Math.ceil(loc*base)) || (base>>1);
};
Card.prototype.transLocate = function ( loca, base ) {
    if(loca == 0)
        return 0;
    return ((loca > 1.0) ? ((loca < base) ? loca : base - (loca%base)) : Math.ceil(loca*base)) || (base>>1);
};
Card.prototype.respondResize = function () {
    this.w = this.locate( this.wa, this.g.canvas.width );
    this.h = this.locate( this.ha, this.g.canvas.height );
    this.x = this.locate( this.xa, this.g.canvas.width-this.w );
    this.y = this.locate( this.ya, this.g.canvas.height-this.h );
    this.xe = this.x+this.w;
    this.ye = this.y+this.h;
    this.maxdim = argmax(this.w,this.h);
    this.mindim = argmin(this.w,this.h);
    this.line = this.locate( this.linea, argmax(8,argmin(16,Math.ceil(this.maxdim>>3))));
    this.prerender();
    //this.draw();
    // call respond resize for all children
    for(ci in this.children)  {
	//this.children[ci].g = this.buffer;
	this.children[ci].respondResize();
    }
};
Card.prototype.respondTranslate = function (x,y) {
    this.x = x;
    this.y = y;
    this.xe = this.x+this.w;
    this.ye = this.y+this.h;
};
Card.prototype.animate = function (destX, speed) {
    if(this.inAnimation == 1)
        return;
    this.destX = destX;
    var dist = this.destX - this.x;
    if(dist == 0)
        return;
    speed = speed || Math.abs(dist);
    var step = ((speed*dist) == 0) ? dist/Math.abs(dist) : (speed*dist); 
    if(speed > 1.0) // take it as pixels otherwise it's ratio of dist
       step /= Math.abs(dist);
    step = (step > 0) ? Math.ceil(step) : Math.floor(step);    
    var firstStep = (speed == dist) ? dist : dist % step;
    this.g.clearRect(this.x,this.y,this.w,this.h);
    this.x += firstStep;
    this.draw();
    var that = this;
    if((that.destX - that.x) != 0) {
        that.inAnimation = 1; 
        that.incX = step;
        that.direction = that.incX/Math.abs(that.incX);
        function astep () {
            that.g.clearRect(that.x,that.y,that.w,that.h);
            that.x += that.incX;
            that.draw();
            if((that.destX - that.x) == 0) { 
                that.stopStepping();
            }
        }
        var stepId = setInterval(astep, 40);
        that.stopStepping = function () {
            clearInterval(stepId);
            that.inAnimation = 0;
        };
    }
};
Card.prototype.prerender = function () { // call once for each object
    this.buffer = renderToCanvas(this.w,this.h,this);
};
Card.prototype.registerRenderer = function(renderer,x,y,s) {
    this.rendargs[renderer] = [x,y,s];
    this.renderers.push(renderer);
};
Card.prototype.registerTextRenderer = function(textRendererProvider,text,s,x,y) {
    var renderer = textRendererProvider.textRender(text,s);
    this.rendargs[renderer] = [x,y,s];
    this.renderers.push(renderer);
    this.text = text;
}
Card.prototype.render = function (gb) { //override for subclasses
    // Construct off screen
    gb.fillStyle = this.fcolor;
    gb.strokeStyle = this.ecolor;
    gb.lineWidth = this.line;
    gb.lineJoin = this.corner;
    gb.strokeRect(this.line>>1,this.line>>1,this.w-this.line,this.h-this.line);
    gb.fillRect(this.line-1,this.line-1,this.w-2*(this.line-1),this.h-2*(this.line-1));
    // call complex renderers
    for(k in this.renderers) {
        var rendargs = this.rendargs[this.renderers[k]]; 
        this.renderers[k].call(this,gb,rendargs[0],rendargs[1],rendargs[2]);
    }
    for(ci in this.children)  {
	this.children[ci].g = gb;
        this.children[ci].respondResize();        
	gb.translate(this.children[ci].x,this.children[ci].y);
	this.children[ci].render(gb);
	gb.translate(-this.children[ci].x,-this.children[ci].y);
    }
    // call child renderers
};
Card.prototype.draw = function () {
    // draw offscreen to screen
    this.g.drawImage(this.buffer,this.x,this.y);
};
Card.prototype.clear = function () {
    this.g.clearRect(this.x,this.y,this.w,this.h);
};
Card.prototype.inside = function (eX,eY) {
    // is the event inside?
    return (eX >= this.x && eX <= this.xe && eY >= this.y && eY <= this.ye); 
};
Card.prototype.respondPush = function (eX,eY) {
    // Can be click or touch
    var ex = eX - this.x;
    var ey = eY - this.y;
    for(ci in this.children) {
	var child = this.children[ci];
        if(child.inside(ex,ey))
		child.respondPush(ex,ey);
    }	
};
Card.prototype.respondDrag = function (sX,sY,dX,dY) {
    // Can be drag or swipe
};
Card.prototype.respondKey = function (keyEvent) {
    // For textbox types
};

