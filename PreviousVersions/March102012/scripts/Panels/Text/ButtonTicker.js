function ButtonTicker (g,x,y,w,h,s) {
   this.parent.constructor.apply(this,[g,x,y,w,h,s]);
   this.g = g;
   this.line = 2;
   this.bcolor = "#ffffff";
   this.ecolor = "#000000";
   this.focolor = "#0000ff";
   this.respondResize(x,y,w,h);
   this.value = [];
   //this.focus();
   this.tCount = 0;
   this.spesh = s || 0;
   this.shadow = [];
   this.empty = new Button();
   this.empty.registerRenderer(r.empty);
   this.empty.prerender();
   this.succesor = this;
}
ButtonTicker.inheritsFrom(TextField);
ButtonTicker.prototype.valueString = function () {
    vs = '';
    for(v in this.value) {
        vs += this.value[v].text;
    }
    return vs;
};
ButtonTicker.prototype.blur = function () {
   this.hasFocus = 0;
   this.scolor = this.ecolor;
   this.redraw();
};
ButtonTicker.prototype.focus = function () {
   this.hasFocus = 1;
   this.scolor = this.focolor;
   this.redraw();
};
ButtonTicker.prototype.respondResize = function (x,y,w,h) {
   this.md = this.g.canvas.width < this.g.canvas.height ? this.g.canvas.width : this.g.canvas.height;
   this.bw = this.md*0.05;
   this.w = w || this.md*0.55;
   this.h = this.md;
   this.x = x || (this.g.canvas.width - this.w)/2;
   this.y = 0;
   this.tx = this.x + 5; 
   this.ey = this.y + this.h;
   this.theight = this.bw;
   this.ty = this.y+this.theight;
};
ButtonTicker.prototype.fill = function(bufList) {
   this.bufList = bufList;
   var i = 0;
    for(bi in this.bufList) {
	var btn = this.bufList[bi];
        this.g.drawImage(btn.buffer,this.ex,i);
        i += btn.w;
    }
};
ButtonTicker.prototype.addButton = function (b) {
    this.value.push(b);
    if(this.h < b.h)
	this.h = b.h;
};
ButtonTicker.prototype.redraw = function () {
   this.clear();
   var met = this.value.length*this.bw;
    if(met < this.h) {
         if(this.spesh)
             this.fill(this.shadow);
         else      
             this.fill(this.value);
    }
    else {
        var slicenum = 1 - Math.ceil(this.h*0.99/met*this.value.length);
         if(this.spesh)
             this.fill(this.shadow.slice(slicenum));
         else      
             this.fill(this.value.slice(slicenum));
    }
};
ButtonTicker.prototype.clear = function () {
   this.g.clearRect(this.ex-1,this.y-1,this.bw+2,this.h+2);
};
ButtonTicker.prototype.preRender = ButtonTicker.prototype.redraw;
ButtonTicker.prototype.draw = ButtonTicker.prototype.redraw;
ButtonTicker.prototype.inside = function(onsubmit) {
  var thist = this;
  return function (e) { 
    var mx = e.offsetX || e.pageX;
    var my = e.offsetY || e.pageY;
    var result = (mx >= thist.x && mx <= thist.ex && my >= thist.y && my <= thist.ey);
    if(result && thist.hasFocus == 0)  {
       thist.focus();
    }
    else if (!result && thist.hasFocus == 1) {
       thist.blur();
       onsubmit();
    }
    return result;
 };
};
ButtonTicker.prototype.tab = function (onsubmit) {
   var thist = this; 
   return function (e) {
      if(thist.hasFocus == 0)
           return 0;
       var code = e.charCode;
       if(code == 0) {
           //worry about making this work later
           //if(thist.isLastOnPage == undefined) {
           e.preventDefault();
           //}
           var key = e.keyCode;
           if(key == 9) {
               thist.blur();
	       thist.succesor.focus();
	       onsubmit();
               return true;
           }
	   if (key == 13) {
               onsubmit();
               return true;
	   }
       }
       return false;
   };
};

ButtonTicker.prototype.addSymbol = function (onsubmit) {
 var thist = this; 
 return function(e) {
    if(thist.hasFocus == 0) {
       return;
    }
    var acode = e.text;
    if(acode == '<') 
    {
          thist.value = thist.value.slice(0,-1);
          if(thist.spesh==1)
              thist.shadow = thist.shadow.slice(0,-1);
    }
    else if (acode == '!') {
          onsubmit();
    }
    else 
    {
	thist.value.push(e);
	if(thist.spesh==1) 
        {
		thist.shadow.push(thist.empty);
        }
    }
    thist.redraw();
   };
};
