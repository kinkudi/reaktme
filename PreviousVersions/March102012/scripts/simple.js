element {
	detached-canvas //where i can do cool rendering
			 //layers are not relevant here because there is no compositing. 
			 //it's just a separate work bench where i can prepare anything i want
	attached-canvas-(layer) //my very own layer
		              //i can do whatever i want here and i will not be destroying anything else
	                      //though i may be overlapping it
	
//elements that are fused together can be on one layer.

//there are a couple of relationships here
//fused children are fused together
//they are on one layer. the parent draw gets called, then the fused children. they are all
// drawn to the same layer. they do not animate separately, they do not move.
//use this for fixed elements....addFixedChild
//another thing is a child element that is non fixed...it is independent but still bound within the box of 
//the parent
//each of these children has their own layer something like
//
new freeChild(requestAttachedCanvas())
new fixedChild(getParentCanvas())

//still each element has its own offscreen canvas too. which is the same size as it's own bounding rectangle.
//a free child has a link to a canvas attached to the dom. when the free child wants to commit its changes
//it pushes and that push goes to that dom layer...that dom layer is provided respecting the parent position.
//it moves with the parent and is not bigger than it.
//so whenever the parent moves. we need to update the position of all its children. 
//this means clearing the parent layer and all the fixed children and recalling that draw
//where draw order is determined by draw time
//and it also means clearing each free child and moving the free childs layer.
//when the free child pushes back to its layer, it will already be in the new place.
//so we only need to move the layer, using css, we don't need to do anything else.
//also when the parent moves, we only need to move its layer( i.e. it's attached canvas).
//so a general useful rule is each elements canvas is only as big as its bounding rectangle. 
//and each elements canvas can move only within the bounds of its parent.
//so each element (except fixed children) has its own canvas.
//moving and animating is done simply by moving around these canvasses -- never by drawing
//if you find yourself calling clear , move and redraw to animate you have it wrong.
//just move the canvas or call css animate to move it.
//the only time we redraw (whether using a clear, or a composite operation) is when the state changes
//on a button press or a text or timer update.
//we should always isolate changing elements in their own canvas.
//so they do not interfere with other elements.
//so the basic mechanism is 
//every element has a canvas that is onscreen and an offscreen one.
//offscreen canvasses are never shared. their are exactly one per element.
//onscreen canvasses are always shared except free children always have their own canvas.
//a free child can have children too, some of them may be fixed others may be free.
//free ness is used when elements will move independently of each other
//or will change or ve otherwise volatile.

//every element
//can have events registered with it
//click, drag, key, resize, load (in some cases)
//elements can also launch flows 
// flows are collections of elements and the glue code that creates them and their event handlers
// mostly elements are atomic. they know where they are supposed to go and go there, waiting for their events, and launching their own flows.
// connections are kept to a minimum.
// it is very modular.
// the emphasis is on easily nesting and positioning of elemtns relatively inside each other
// requesting animations
// requesting color animations
// requesting size animations 
// with animations being taken to mean 'changes over time'
// to create a menu it should be as simple as

var menu = new Element(center)
var list = new Element(left)
menu.add(list)
list.add(new Element(top))
list.add(new Element(top))

panel = new Element(right)
button = new Element(center,right)
button2 = new Element(center,right)
textField = new Element(left)
panel.add(button)
panel.add(button2)
panel.add(textField)
button.action.add( textField.timeValue.increment )
button2.action.add( textField.timeValue.decrement )
button.image.add( renderer.uparrow )
button.image.add( renderer.downarrow )
button3 = new Element(center, right)
panel.add(button3)
button3.image.add( renderer.cloud_with_arrow )
textField2 = new Element(top)
panel.add(textField2)
button3.action.add( server.meeting.arrange.time.submit(textField2.callback) )
//nothing has been draw yet
//and no event handlers are listening
//so we grab the top element and go
menu.add(panel)
menu.launch()
//there it's lke 10 lines of javascript and we made a new menu
//which gets a complex user input and sends it to the server
//awesome
//everything like layout and stuff is handled generically back there
//and we create a server to meet our application flow.
//a js server model
//what else? 
//launch will draw everything properly
//and also launch all the correct even listeners
//it should be that simple
//that means that a new feature can be up and running in 10 minutes. just like my current work flow
//suits
//in the same time i can define a wsgi hook at the server
//and a data model
//and that's it
//new feature deployed, committed and tested in 30 minutes.
//this is the framework i need and want.


































function requestOffscreenCanvas(width,height) {
	var newc = document.createElement('canvas');
	var newg = newc.getContext('2d');
	newg.canvas.width = width;
	newg.canvas.height = height;
	return newg;
};
function Simple(g,x,y,w,h) {
	this.xr = x || 0.5;
	this.yr = y || 0.5;
	this.wr = w || 0.9;
	this.hr = h || 0.9;
	this.drawContexts = [];
	this.setDrawContext(g || requestOffscreenCanvas(window.innerWidth,window.innerHeight));
	this.makeReal();
	this.childCanvas = requestOffscreenCanvas(this.w,this.h);
	this.children = [];
};
Simple.prototype.goLive = function() {
	this.live = 1;
};
Simple.prototype.offAir = function() {
	this.live = 0;
};
Simple.prototype.setDrawContext = function(dc) {
	if(this.g != undefined && this.g != null)
		this.drawContext.push(this.g);
	this.g = dc;
};
Simple.prototype.removeDrawContext = function() {
	if(this.drawContexts.length > 0)
		this.g = this.drawContexts.pop();
	else
		this.g = requestCanvas(this.w,this.h);
};
Simple.prototype.makeReal = function() {
	this.w = this.wr * this.g.canvas.width;
	this.x = this.xr * (this.g.canvas.width-this.w);
	this.h = this.hr * this.g.canvas.height;
	this.y = this.yr * (this.g.canvas.height-this.h);
};
Simple.prototype.delta = function(xamount,yamount) { // value between 0 and 1 proportion of parent
	var xreal_amount = xamount*this.g.canvas.width;
	this.wr = this.wr + xamount;	
	this.x = this.x + xreal_amount;
	this.absolutex = this.parent.X() + this.x;
	var yreal_amount = yamount*this.g.canvas.height;
	this.hr = this.hr + yamount;
	this.y = this.y + yreal_amount;
	this.absolutey = this.parent.Y() + this.y;
	if(this.live==1)
		this.redraw();
	notifyChildren_delta(xreal_amount,yreal_amount);
}; 
Simple.prototype.notifyChildren_delta = function(rax,ray) {
	for(cname in this.children)
		this.children[cname].notify_delta(rax,ray);
}; 
Simple.prototype.notify_delta = function(rax,ray) {
	this.absolutex += rax;
	this.absolutey += ray;
}; 
Simple.prototype.X = function() {
	return this.absolutex;
};
Simple.prototype.Y = function() {
	return this.absolutey;
};
Simple.prototype.draw = function() {
	this.globalCanvas.drawImage(this.sharedCanvas,this.X(),this.Y());
};
Simple.prototype.clear = function () {
	this.globalCanvas.clearImage(this.X()-2,this.Y()-2,this.w+4,this.h+4);
};
Simple.prototype.addChild = function(cname,c) {
	this.children[cname] = c;
	c.setDrawContext(this.childCanvas);
};
Simple.prototype.removeChild = function(cname) {
	delete this.children[cname];
}; 
Simple.prototype.drawChild = function(cname) {
	this.children[cname].draw();
};
Simple.prototype.clearChild = function(cname) {
	this.children[cname].draw();
};
Simple.prototype.drawChildren = function() {
	for(cname in this.children)
		this.drawChild(cname);
};
Simple.prototype.clearChildren = function() {
	for(cname in this.children)
		this.clearChild(cname);
};

var s = new Simple();
s.draw();
