import webapp2
from models.Keys import LoginInstance
from models.Keys import login_attempt
from IsoCountryCodes import COUNTRY

class Bouncer(webapp2.RequestHandler):
	def post(self):
		self.response.headers['access-control-allow-origin'] = '*'
		self.response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
		name = self.request.POST.get('name')
		secret = self.request.POST.get('secret')
		uagent = self.request.headers.get('User-Agent')
		country = self.request.headers.get('X-AppEngine-country')
		if country is not None:
			country = COUNTRY.get(country)
		if country is None:
			country = 'Earth'
		ip = self.request.remote_addr
		login_instance = login_attempt(name,secret,ip,uagent,country)
		if login_instance.success_status:
			self.response.out.write("Login successful")
		else:
			self.response.out.write("Login was not success")

	def get(self):
		self.redirect('http://reakt.me/')

app = webapp2.WSGIApplication([
	(r'/reaktmeDoor/.*',Bouncer)
	])

def main():
	app.run()

if __name__ == "__main__":
	main()


