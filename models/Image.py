import base64
import random
from google.appengine.ext import db
from google.appengine.ext import blobstore
from google.appengine.api import files
from google.appengine.api import images

#Everything to do with images
keyLow = 1000000
keyHigh = 2000000000

class RImage(db.Model):#just a wrapper on the blob in case of corruption
	usernumber=db.IntegerProperty(indexed=False)
	when_uploaded=db.DateTimeProperty(auto_now_add=True,indexed=False)
	src=blobstore.BlobReferenceProperty(indexed=False)
	w=db.IntegerProperty(indexed=False)
	h=db.IntegerProperty(indexed=False)

def get_image_container_and_key(usernumber,w,h):
	key1 = random.randint(keyLow,keyHigh)
	key2 = random.randint(keyLow,keyHigh)
	key = '_'.join([`usernumber`,`key1`,`key2`,`w`,`h`])
	ei = RImage.get_by_key_name(key)
	while ei is not None:
		key2 = random.randint(keyLow,keyHigh)
		key = '_'.join([`usernumber`,`key1`,`key2`,`w`,`h`])
	ei = RImage(key_name=key)
	ei.w = w
	ei.h = h
	ei.usernumber = usernumber
	return ei,key			

def add(usernumber,src,w,h):
	img_container,key = get_image_container_and_key(usernumber,w,h)
	file_name = files.blobstore.create(mime_type='image/png')
	raw_src = base64.b64decode(src)
        with files.open(file_name,'w') as fo:
		fo.write(raw_src)
	files.finalize(file_name)
	blob_key = files.blobstore.get_blob_key(file_name)
	img_container.src=blob_key
	img_container.url=get_serving_url(blob_key)
	img_container.put()
	return key
	
