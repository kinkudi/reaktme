from google.appengine.ext import db

#everything to do with user interaction

class Radical(db.Model): #key is user number
	rimages=db.ListProperty(db.Key,indexed=False) #rimages
	collisions=db.ListProperty(db.Key,indexed=False) #said hi
	reactions=db.ListProperty(db.Key,indexed=False) #arranged and completed a meet
        compounds=db.ListProperty(db.Key,indexed=False) #promoted to meetable 
	
class Collision(db.Model):
	left=db.ReferenceProperty(Radical,indexed=False)
	right=db.ReferenceProperty(Radical,indexed=False)
	when_collided=db.DateTimeProperty(auto_now_add=True,indexed=False)

class Reaction(db.Model):
	left=db.ReferenceProperty(Radical,indexed=False)
	right=db.ReferenceProperty(Radical,indexed=False)
	when_reacted=db.DateTimeProperty(auto_now_add=True,indexed=False)
	when_planned=db.DateTimeProperty(indexed=False)
	where_planned=db.StringProperty(indexed=False)

class Compound(db.Model):
	left=db.ReferenceProperty(Radical,indexed=False)
	right=db.ReferenceProperty(Radical,indexed=False)
	when_stabilised=db.DateTimeProperty(auto_now_add=True,indexed=False)
	reaction_history=db.ListProperty(db.Key,indexed=False) # list of reactions accompished
	reaction_plan=db.ListProperty(db.Key,indexed=False) # list of planned reactions

