function Button() { 

    var args = Array.prototype.slice.call(arguments);
    this.parent.constructor.apply(this,args);
    this.actions = [];
}
Button.inheritsFrom( Card );
Button.prototype.registerAction = function(action) {
    this.actions.push(action);
};
Button.prototype.execute = function() {
    for(ai in this.actions)
        this.actions[ai].execute();
};
