
var bc = document.getElementById('bc');
bc.style.position = "fixed";
bc.style.zIndex = "102";
var gbc = bc.getContext('2d');
bc.respondResize();

var cr = new CropBox(gbc);
cr.draw();

resizables = []
resizables.push(bc);
resizables.push(cr);

var mouseDown = 0;
var crdrag = 0;

function dragHandler(e) {
    if(mouseDown == 0)
        return;
    switch(crdrag) {
        case(0):
            break;
        case(1):
            cr.resizeH(e.clientY);
            break;
        case(2):
            cr.resizeW(e.clientX);
            break;
    }
};
function registerDown(e) {
    mouseDown = 1;
    crdrag = cr.touchvalue(e.clientX,e.clientY);
};
function registerUp(e) {
    mouseDown = 0;
};
function resizeHandler(e) {
    for(ri in resizables) {
        var dg = resizables[ri];
        if( dg == null || dg == undefined )
            continue;
        else
             dg.respondResize();
    }
    cr.resizeDraw(); 
};
document.addEventListener('mousedown', registerDown, false);
document.addEventListener('mouseup', registerUp, false );
document.addEventListener('mousemove', dragHandler, false );
window.addEventListener('resize', resizeHandler, false );

