function Draggable(p,src) {
	this.p = p;
        this.src = src || null;
        if(this.src != null)
            this.ratio = this.src.width/this.src.height;
        else
            this.ratio = 1.618;
	this.respondResize();
};
Draggable.prototype.draw = function () {
    if(this.src != null)
        this.p.drawImage(this.src,this.x,this.y,this.w,this.h);
    else
        this.p.fillRect(this.x,this.y,this.w,this.h);
};
Draggable.prototype.clear = function () {
    this.p.clearRect(this.x-1,this.y-1,this.w+2,this.h+2);
};
Draggable.prototype.respondDrag = function(dx,dy) {
    this.clear();
    if(this.p.canvas.insideW(dx))
        this.x = dx - this.px;
    if(this.p.canvas.insideH(dy))
        this.y = dy - this.py;
    this.draw();
};
Draggable.prototype.respondResize = function() {
        this.oh = (this.p.canvas.height*2)/3;
        this.ow = this.oh*this.ratio;
        this.h = this.oh;
        this.w = this.ow;
        this.ox = (this.p.canvas.width-this.w)/2;
        this.oy = (this.p.canvas.height-this.h)/2;
        this.x = this.ox;
        this.y = this.oy;
        this.clear();
        this.draw();
};
Draggable.prototype.inside = function(mx,my) {
    return ( mx >= this.x && mx <= (this.x+this.w) && my >= this.y && my <= (this.y+this.h));
};
Draggable.prototype.rememberDragPosition = function(px,py) {
    this.px = px - this.x;
    this.py = py - this.y; 
};

