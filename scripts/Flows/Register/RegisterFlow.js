function RegisterFlow () {
    var args = Array.prototype.slice.call(arguments);
    this.parent.constructor.apply(this,args);
    this.fcolor = "hsla(50,100%,70%,1.0)";
    this.ecolor = "hsla(50,100%,50%,1.0)";
    this.field = new TextField(this.g,this.x+this.w*0.15,this.y+this.h*0.12,this.w*0.7,this.h*0.2);
    this.field2 = new TextField(this.g,this.x+this.w*0.15,this.y+this.h*0.40,this.w*0.7,this.h*0.2,1);
    this.field3 = new TextField(this.g,this.x+this.w*0.15,this.y+this.h*0.68,this.w*0.7,this.h*0.2,1);
    this.prerender();
}
RegisterFlow.inheritsFrom( Card );
RegisterFlow.prototype.draw = function () {
    this.parent.draw.call(this,arguments);
    this.field.redraw();
    this.field2.redraw();  
    this.field3.redraw();
};
RegisterFlow.prototype.respondResize = function () {
    this.parent.respondResize.call(this,arguments);
    this.prerender();
    if(this.field && this.field2 && this.field3) {
        this.field.respondResize(this.x+this.w*0.15,this.y+this.h*0.12,this.w*0.7,this.h*0.2);    
        this.field2.respondResize(this.x+this.w*0.15,this.y+this.h*0.4,this.w*0.7,this.h*0.2);    
        this.field3.respondResize(this.x+this.w*0.15,this.y+this.h*0.68,this.w*0.7,this.h*0.2);    
        this.draw();
    }
};

