aCard = new Card(g,0.5,0.5,0.6,0.6);
aClock = new Clock(g,0.5,0.5,0.4,0.4);
var upbutton = new Button(g,0.5,0.2,0.2,0.1);
var downbutton = new Button(g,0.5,0.8,0.2,0.1);
upbutton.prerender();
downbutton.registerAction( new Action (
	function () {
		alert("Down");
	}
));
downbutton.prerender();

var atime = new Card(g,0.25,0.65,0.2,0.4,undefined,undefined,undefined,'hsla(180,100%,50%,1.0)');
var a = new Button(g,0.25,0.25,0.2,0.2);
a.registerTextRenderer(r,'!',0.3);
a.registerAction( new Action( //should make a flow
	function () {
		//alert("Today");
		atime.draw();
		var atimeh = function(e) {
			var ex = e.pageX || e.clientX;
			var ey = e.pageY || e.clientY;
			if(atime.inside(ex,ey))
				atime.respondPush(ex,ey);	
		}
		document.addEventListener('click',atimeh,true);
	}
));
atime.addChild(upbutton);
atime.addChild(downbutton);
atime.addChild(aClock);
atime.prerender();
a.prerender();
upbutton.registerAction( new Action (
	function () {
		aClock.upTime(0,15);
		atime.prerender();
		atime.clear();
		atime.draw();
	}
));

var btime = new Card(g,0.5,0.65,0.2,0.4,undefined,undefined,undefined,'hsla(180,100%,50%,1.0)');
btime.prerender();

var b = new Button(g,0.5,0.25,0.2,0.2);
b.registerTextRenderer(r,'+1',0.3);
b.registerAction( new Action(
	function () {
		//alert("Tomorrow");
		btime.draw();
		var atimeh = function(e) {
			var ex = e.pageX || e.clientX;
			var ey = e.pageY || e.clientY;
			if(btime.inside(ex,ey))
				btime.respondPush(ex,ey);	
		}
		document.addEventListener('click',atimeh,true);
	}
));
btime.addChild(upbutton);
btime.addChild(downbutton);
btime.prerender();
b.prerender();

var ctime = new Card(g,0.75,0.65,0.2,0.4,undefined,undefined,undefined,'hsla(180,100%,50%,1.0)');
ctime.prerender();
var c = new Button(g,0.75,0.25,0.2,0.2);
c.registerTextRenderer(r,'+1+1',0.3);
c.registerAction( new Action(
	function () {
		//alert("The next day");
		ctime.draw();
		var atimeh = function(e) {
			var ex = e.pageX || e.clientX;
			var ey = e.pageY || e.clientY;
			if(ctime.inside(ex,ey))
				ctime.respondPush(ex,ey);	
		}
		document.addEventListener('click',atimeh,true);
	}
));
ctime.addChild(upbutton);
ctime.addChild(downbutton);
ctime.prerender();
c.prerender();

aCard.addChild(c);
aCard.addChild(b);
aCard.addChild(a);
aCard.prerender();
aCard.draw();

