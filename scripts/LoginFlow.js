function LoginFlow () {
    var args = Array.prototype.slice.call(arguments);
    this.parent.constructor.apply(this,args);
    this.fcolor = "hsla(40,100%,70%,1.0)";
    this.ecolor = "hsla(40,100%,50%,1.0)";
    this.linea = 0.2;
    this.respondResize();
    this.field = new TextField(this.g,this.x+this.w*0.17,this.y+this.h*0.23,this.w*0.66,this.h*0.2);
    this.field2 = new TextField(this.g,this.x+this.w*0.17,this.y+this.h*0.56,this.w*0.66,this.h*0.2,1);
}
LoginFlow.inheritsFrom( Card );
LoginFlow.prototype.draw = function () {
    this.parent.draw.call(this,arguments);
    this.field.redraw();
    this.field2.redraw();  
};
LoginFlow.prototype.respondResize = function () {
    this.wa = (g.canvas.width < 200 ? 0.99 : (g.canvas.width > 640 ? 480 : 0.67));
    this.ha = 0.5;
    this.parent.respondResize.call(this,arguments);
    this.prerender();
    if(this.field && this.field2) {
        this.field.respondResize(this.x+this.w*0.15,this.y+this.h*0.23,this.w*0.7,this.h*0.2);    
        this.field2.respondResize(this.x+this.w*0.15,this.y+this.h*0.56,this.w*0.7,this.h*0.2);    
        this.draw();
    }
};

