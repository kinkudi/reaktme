function Setup () { //A particular setup for a meeting
	var args = Array.prototype.slice.call(arguments);
	this.parent.constructor.apply(this,args);
	this.timedate = null;
	this.place = null;
	this.left = null;
	this.right = null;
	this.special = null; // reserved for future
}
Setup.inheritsFrom(Card);

