function SetupChooser() { // a card to choose/negotiate/create setup
	var args = Array.prototype.slice.call(arguments);
	this.parent.constructor.apply(this,args);
	this.timedateLow = null;
	this.timedateHigh = null;
	this.timedatepicker = null; // the dialog 
	this.places = {};
	this.left = null;
	this.right = null;
	this.change_history = [];
	this.respondResize();
	this.days = [];
};
SetupChooser.inheritsFrom(Card);

