function TextField (g,x,y,w,h,s) {
   this.g = g;
   this.line = 2;
   this.bcolor = "#ffffff";
   this.ecolor = "#000000";
   this.focolor = "#0000ff";
   this.respondResize(x,y,w,h);
   this.value = "";
   this.blur();
   this.tCount = 0;
   this.spesh = s;
   this.shadow = "";
   this.successor = this;
}
TextField.prototype.blur = function () {
   this.hasFocus = 0;
   this.scolor = this.ecolor;
   this.redraw();
};
TextField.prototype.focus = function () {
   this.hasFocus = 1;
   this.scolor = this.focolor;
   this.redraw();
};
TextField.prototype.respondResize = function (x,y,w,h) {
   this.x = x;
   this.w = w;
   this.y = y;
   this.h = h;
   this.ex = this.x + this.w;
   this.tx = this.x + 5; 
   this.ey = this.y + this.h;
   this.theight = 6*(this.h>>3);
   this.ty = this.y+this.theight;
   this.font = this.theight + "px Courier-New, Courier, monospace";
};
TextField.prototype.redraw = function () {
   this.clear();
   this.g.lineWidth = this.line;
   this.g.strokeStyle = this.scolor;
   this.g.fillStyle = this.bcolor;
   this.g.fillRect(this.x,this.y,this.w,this.h);
   this.g.strokeRect(this.x,this.y,this.w,this.h);
   this.g.font = this.font;
   this.g.fillStyle = this.ecolor;
    var met = this.g.measureText(this.value).width;
    if(met < this.w) {
         if(this.spesh)
             this.g.fillText(this.shadow,this.tx,this.ty);
         else      
             this.g.fillText(this.value,this.tx,this.ty);
    }
    else {
        var slicenum = 1 - Math.ceil(this.w*0.99/met*this.value.length);
        if(this.spesh)
            this.g.fillText(this.shadow.slice(slicenum),this.tx,this.ty);
        else
            this.g.fillText(this.value.slice(slicenum),this.tx,this.ty);
    }
};
TextField.prototype.clear = function () {
   this.g.clearRect(this.x-1,this.y-1,this.w+2,this.h+2);
};
TextField.prototype.inside = function() {
  var thist = this;
  return function (e) { 
    var mx = e.offsetX || e.pageX;
    var my = e.offsetY || e.pageY;
    var result = (mx >= thist.x && mx <= thist.ex && my >= thist.y && my <= thist.ey);
    if(result)  {
       if(thist.value === "Click here") {
          thist.value = "";
          thist.redraw();
       }
       thist.focus();
    }
    else {
       thist.blur();
    }
    return result;
 };
};
TextField.prototype.tab = function () {
   var thist = this; 
   return function (e) {
       if(thist.hasFocus == 0)
           return 0;
       var code = e.charCode;
       if(code == 0) {
           e.preventDefault();
           var key = e.keyCode;
           if(key == 9) {
               thist.blur();
               thist.succesor.focus();
           }
       }
       return 1;
   };
};
TextField.prototype.addLetter = function () {
 var thist = this; 
 return function(e) {
    if(thist.hasFocus == 0)
       return;
    var code = e.charCode;
    if(code == 0) {
        switch(e.keyCode) {
            case(8):
                thist.value = thist.value.slice(0,-1);
                if(thist.spesh)
                    thist.shadow = thist.shadow.slice(0,-1);
                break;
        }
    }
    else {
        var char = String.fromCharCode(code);
        thist.value += char;
        if(thist.spesh)
            thist.shadow += "*";
    }
    thist.redraw();
  };
};

