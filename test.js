"use strict";
//var img;
//i just had an idea
//sinca the drawing context is where it all goes on
// i should enclose the canvas class
//but also i should give each canvas a unique uri
//that i can address and find them by
//this will be useful later
//the main parts of the program
//like canvas, panel, event listeners,renderers
//shoud all be accessible by a unique uri
//it really makes sense
if(typeof(String.prototype.trim) === "undefined")
{
    String.prototype.trim = function() 
    {
	return String(this).replace(/^\s+|\s+$/g, '');
    };
}
String.prototype.splitf = function (regex) {
	var regexp = new RegExp(regex);
	var match = regexp.exec(this);
	if(!match || match == null)
		return [this];
	var index = this.indexOf(match);
	return [this.slice(0,index),this.slice(index+match.length)]		
};
String.prototype.rjust = function( width, padding ) {
	padding = padding || " ";
	padding = padding.substr( 0, 1 );
	if( this.length < width )
		return padding.repeat( width - this.length ) + this;
	else
		return this;
};
String.prototype.repeat = function( num ) {
	for( var i = 0, buf = ""; i < num; i++ ) buf += this;
		return buf;
};
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};
function CanvasServer() {
	//all below code goes in here :)
	return this;
};
var GLOBALZINDEX = 0;
function requestNextZIndex() {
	var returnablez = GLOBALZINDEX;
	GLOBALZINDEX += 1;
	return returnablez;
};
var BODY = document.getElementsByTagName('body')[0];
Function.prototype.expands = function( baseClassOrObject ) {
    if ( baseClassOrObject.constructor == Function ) {
        this.prototype = new baseClassOrObject();
        this.prototype.constructor = this;
        this.prototype.base = baseClassOrObject.prototype;
    }
    else {
        this.prototype = baseClassOrObject;
        this.prototype.constructor = this;
        this.prototype.base = baseClassOrObject;
    }
    return this;
};
function requestPrivateOffscreenContext(width,height) {
	var xwidth = width || window.innerWidth;
	var yheight = height || window.innerHeight;
	var newc = document.createElement('canvas');
	var newg = newc.getContext('2d');
	newg.canvas.width = xwidth;
	newg.canvas.height = yheight;
	return newg;
};
function attach(context) {
	BODY.appendChild(context.canvas);
	return context;
};
function goLive(context) {
	context.canvas.style.display = 'inline';
	return context;
};
function goDark(context) {
	context.canvas.style.display = 'none';
	return context;
};
function requestDistinctOnscreenContext(width,height) {
	var newprivateoffscreen = requestPrivateOffscreenContext(width,height);
	newprivateoffscreen.canvas.style.position = 'fixed';
	newprivateoffscreen.canvas.style.top = 0;
	newprivateoffscreen.canvas.style.left = 0;
	newprivateoffscreen.canvas.style.zIndex = requestNextZIndex();
	goDark(newprivateoffscreen);	
	return attach(newprivateoffscreen);
};
function Locate(xpos,ypos,xsz,ysz) {
	xpos = xpos || 0.0;
	ypos = ypos || 0.0;
	xsz = xsz || 1.0;
	ysz = ysz || 1.0;
	this.rel = new Relative(xpos,ypos,xsz,ysz);
	return this;
};
Locate.prototype.inContext = function(g) {
	this.abs = new Absolute(this.rel,g);
	return this;
};
function Relative(xpos,ypos,xsz,ysz) {
	this.x = xpos;
	this.y = ypos;
	this.xsz = xsz;
	this.ysz = ysz;
	return this;
};
function Absolute(rel,context) {
	this.maxx = context.canvas.width;
	this.maxy = context.canvas.height;
	this.x = Math.round(rel.x * this.maxx);
	this.xsz = Math.round(rel.xsz * this.maxx);
	this.endx = this.x+this.xsz;
	this.y = Math.round(rel.y * this.maxy);
	this.ysz = Math.round(rel.ysz * this.maxy);
	this.endy = this.y+this.ysz;
	return this;
};
function toArray(conv) {
	return Array.prototype.slice.call(conv);
};
Function.prototype.as = function() {
	if (arguments.length < 1)
		return this;
	var __method = this;
	var args = toArray(arguments);
	return function() {
		var largs = toArray(arguments);
		return __method.apply(this,largs.concat(args));
	}
}
function Element() {
	this.locate() 
	this.privateOffscreenContext = requestPrivateOffscreenContext();
	this.live = 0;
	this.x = 0;
	this.y = 0;
	this.imageloading = 0;
	this.launchable = 0;
	this.waitq = []
	return this;
};
Element.prototype.locate = function(xpos,ypos,xsz,ysz) {
	this.loc = new Locate(xpos,ypos,xsz,ysz);
	this.launchable = 0;
	return this;
};
Element.prototype.at = function(gcontext) {
	this.onscreenContext = gcontext;
	this.giveContext(gcontext);
	this.launchable = 0;
	return this;
};
Element.prototype.add = function(child) {
	if(this.children == undefined)
		this.children = [];
	if(this.sharedChildContext == undefined)
		this.sharedChildContext = requestPrivateOffscreenContext();
	this.children.push(child);
	child.giveContext(this.sharedChildContext);
	this.launchable = 0;
	return this;
};
Element.prototype.giveContext = function(aContext) {
	this.loc.inContext(aContext);
	this.privateOffscreenContext.canvas.width = aContext.canvas.width;
	this.privateOffscreenContext.canvas.height = aContext.canvas.height;
	this.launchable = 0;
	return this;
};
Element.prototype.warmup = function() {
	//alert('Attempting warmup ... ');
	if(this.live == 1)
		return this;
	////alert('Warming up');
	goLive(this.onscreenContext);
	this.live = 1;
};
Element.prototype.cooldown = function() {
	if(this.live == 0)
		return this;
	goDark(this.onscreenContext);
	this.live = 0;
};
Element.prototype.renders = function(renderer,ondone) {
	if(this.renderers == undefined)
		this.renderers = []
	//alert('La');
	this.renderers.push({ 'renderer' : renderer, 'ondone' : ondone });
	//alert('Ja');
	this.launchable = 0;
	//this.draw();
	return this;
};
Element.prototype.draw = function() {
	//alert('Attempting draw...');
	if(this.launchable == 1)
		return this;
	//alert('Drawing ' + this.renderers.length);
	for(var ri in this.renderers) {
		//alert('Ha ' + ri);
		var rendermsg = this.renderers[ri];
		rendermsg['renderer'].renderTo(this,this.privateOffscreenContext,rendermsg['ondone']);
 	}
	return this;
};
Element.prototype.launch = function() {
	//alert('Attempting launch...');
	if(this.launchable == 0)
		this.draw();
	if(this.live == 0)
		this.warmup();
	//alert('Launching from ' + this.privateOffscreenContext.canvas );
	this.onscreenContext.drawImage(this.privateOffscreenContext.canvas,this.x,this.y);
	//for(var child in this.children)
	//	this.children[child].launch();
	//if(this.sharedChildContext != undefined)
	//	this.onscreenContext.drawImage(this.sharedChildContext.canvas,this.x,this.y);
	return this;
};

function Free() {
	this.base.constructor.apply(this);
	this.at(requestDistinctOnscreenContext());
	return this;
};
Free.expands(Element);

function Fixed() {
	this.base.constructor.apply(this,arguments);
	return this;
};
Fixed.expands(Element);
Fixed.prototype.giveContext = function(sharedContextFromParent) {
	this.base.giveContext.call(this,sharedContextFromParent);
	this.onscreenContext = sharedContextFromParent;
	return this;
};
Fixed.prototype.ensureReady = function() {
	if(this.onscreenContext == undefined)
		this.giveContext(ENV.main);
	return this;
};
Fixed.prototype.warmup = function() {
	return this.ensureReady().base.warmup.call(this,arguments);
};
Fixed.prototype.launch = function() {
	//alert('Launch from fixed...');
	return this.ensureReady().base.launch.call(this,arguments);
};

//he basic panel class appears to be defined now
// i need to test nesting
// add renderers (for vectors, text and images)
// add event listeners
// and server calls
// and decoration like borders
// and animation
// why am i redoing this?
// to make the foundation, the framework really sound
// so i can be endlessly expressive later
// then build predefined subtypes 
// RIGHT i have the relative framework handled
// and i have an algorithm for doing 9 positions layout
// 1. assign the element to the box specified by its 9 pos layout
// 2. if this box is taken, move left if in center, 
//    otherwise move down or up.
//   if this subsequent search is still full
// resize the elements in that row/column to allow us to fit in
// if they can't be resized any more (due to minimums hit)
// move again. If all spots are taken 
// force the lowest priority element to be resized (we can set priority)
// but it is automatically set at larger for any element that displays input or // or text or photos and smaller for anything that displayes vector rendered buttons
// we can also request the containing parent be resized.
// that's it
// this will enable us to specify left,middle and have all the relative rescalable
// co-ordinates taken care of behind the screnes
// cancel the above. such a layout algorithm is beyond me right now
// and besides taking care of co-ords at design gives me explicit control
// i can probably also use currying for many funcitons including the free box search for different spots
// i can probably use currying for relative calculation to
// after i have got this i just have to make it tight with nesting
// and then the basic panel class is defined
// next to add will be specific support for photos, resizing
// and server calls
// then i will add event listeners like dragging , clicking keying
// then i will add renderers both text and vector graphics.
// when this is done the panel class , and basically the whle program framewrok will be defined
// i will then subclass / curry this functionality to support specif useful input cases and flows that i think are cool
// rememebr no negative feedback to user , just ignore events that don't do anytnhing,,bt always feed back positive when soemthing happens
// after that i will look into leveraging gpu support through the api for animation
// and also look into deining the overarching state , transitions classes framework for easily defining UI interacitons and flows

// then this really tight, tiny framework will be ready to rock
//then we need a way to convert things like
//top left etc to actual positions
//in a way such that where current objects are on the canvas
//is remembered and accounted for
//we can't actuall use absolute co ordinate
//plus whatever we draw we need to store
//as instructions
//so we can redraw in on resize
//we need to use relative co-ordinates and sizes
//expressed as percentages of the given context
//in other words the containing context
//so we need a few things
// a renderer class
// an ability to store separately the relative co-ordinates
// and the absolute ones
//once we have this, when we resize the canvas (any one)
// by say giving a context or changing the context for an element
// what we do is we can simply redraw those render on the new canvas
// exactly consistent
// since they have relative scores

// so we need to have a renderer
// relative score
// and a hook for when we resize either by onresize
// or when we actually just resize the canvas
// but apart from that it's working well 


/**
function RenderAction(action) {
};
RenderAction.prototype.background = function() { 
	//alert('return bg function called');
	//color = 'hsla(30,100%,50%,1.0)';
	return function(context,color) {
		//alert('bg function called context: ' + context + ', color: ' + color);
		context.fillStyle = color;
		//alert(this.loc.abs.x + ',' + this.loc.abs.xsz);
		context.fillRect(this.loc.abs.x,this.loc.abs.y,this.loc.abs.xsz,this.loc.abs.ysz);	
        };
};
RenderAction.prototype.square = function() {
	return function(context,color) {
		context.fillStyle = color;
		context.fillRect(this.loc.abs.x,this.loc.abs.y,this.loc.abs.xsz,this.loc.abs.xsz);
	};
};
**/
//render.fill().rect()
//render.stroke().rect()
//some things that we will share 
function RenderProvider() {
	//this does the acutal inerfacing with canvas API
	return this;
};
RenderProvider.prototype.drawImage = function(srcElement,context,state,detail,loadhook) {
	if(!detail)
		return;
	//alert('in drawImage, loadhook is ' + loadhook);
	switch(detail.constructor) {
		case String:
			var img = new Image();
			img.onload = function() {
				//alert('in onload');
				context.drawImage(img,0,0,context.canvas.width,context.canvas.height);
				loadhook();
				//srcElement.draw();
				//alert('about to call loadhook');
				//srcElement.launch(); if i do it here
				// other glyphs after won't be drawn/launched
			};
			img.src = detail;
			break;
		case HTMLImageElement:
			if(detail.complete) {
				//alert('image already complete');
				context.drawImage(detail,0,0,context.canvas.width,context.canvas.height);
				//alert('about to call loadhook');
				loadhook();
			}
			else {
				
				detail.onload = function() {
					//alert('in onload (img)');
					context.drawImage(detail,0,0,context.canvas.width,context.canvas.height);
					//srcElement.draw();
					//alert('about to call loadhook');
					loadhook();	
				};
			}
			break;
		default:
			break;
	}
};
// decide grmmar (json if avail of just array or javascript object)
RenderProvider.prototype.parseTextDetail = function(srcElement,context,detail) {
	// Detail can have the following formats
	// [@ number] text [@ number,number]
        // text [@ number number,number]
	// the first number is always font size in screen ratio, 
        // the second and third number are the x and y positions
	// we can escape a @ in the text with !
	var xdefault = 0.5;
	var ydefault = 0.5;
	var sizedefault = 0.1;
        var raw = detail.trim().replace('!@', '^^^REPLACEDAT^^^');
	var numberStart = /^@ *\d+\.\d+/;
	var atAt = /@/;
	var atSpace = /\s+/;
	var atComma = /,/;
	var width = srcElement.loc.abs.xsz;
	var height = srcElement.loc.abs.ysz;
	var md = width < height ? width : height; 
	var font = 'px Helvetica'; //our font
        if (raw.match(numberStart)) {
		raw = raw.substring(1).trim();
        	var raws = raw.splitf(atSpace);
		//alert(raws);
		var fontSize = isNumber(raws[0].trim()) ? parseFloat(raws[0].trim()) : sizedefault; // default is 0.1
		var raws2 = raws[1].splitf(atAt);
		var text = raws2[0].trim();
		if(text.length == 0)
			return;	
		var xpos = xdefault;
		var ypos = ydefault;
		if(raws2.length > 1) {
			var raws3 = raws2[1].splitf(atComma);
			xpos = isNumber(raws3[0].trim()) ? parseFloat(raws3[0].trim()) : xpos;
			if(raws3.length > 1) 
				ypos = isNumber(raws3[1].trim()) ? parseFloat(raws3[1].trim()) : ypos; 
		}	
	}
	else {
		var raws = raw.splitf(atAt);
		var text = raws[0].trim();
		if(text.length == 0)
			return;		
		var ypos = ydefault;
		if(raws.length > 1) {
			var raws2 = raws[1].trim().splitf(atSpace);
			if(raws2[0].charAt(raws2[0].length-1) == ',') {
				var xpos = isNumber(raws2[0].trim()) ? parseFloat(raws2[0].trim()) : ydefault; 
			}
			else {
				var fontSize = isNumber(raws2[0].trim()) ? parseFloat(raws2[0].trim()) : sizedefault;
			}
			if(raws2.length > 1) {
				var raws3 = raws2[1].trim().splitf(atComma);
				if(raws3.length < 2) {

					if(xpos)
						var ypos = isNumber(raws3[0].trim()) ? parseFloat(raws3[0].trim()) : ydefault;
					else
						var xpos = isNumber(raws3[0].trim()) ? parseFloat(raws3[0].trim()) : xdefault;					
				}	
				else {
					if(xpos)
						var fontSize = xpos;
					else
						var xpos;
					xpos = isNumber(raws3[0].trim()) ? parseFloat(raws3[0].trim()) : xdefault; 
					ypos = isNumber(raws3[1].trim()) ? parseFloat(raws3[1].trim()) : ydefault;	
				}	
			}
			else {
				var raws3 = raws2[0].trim().split(atComma);
				if(raws3.length > 1) {
					if(xpos)
						var fontSize = xpos;
					else
						var xpos;
					xpos = isNumber(raws3[0].trim()) ? parseFloat(raws3[0].trim()) : xdefault;
					ypos = isNumber(raws3[1].trim()) ? parseFloat(raws3[1].trim()) : ydefault;	
				}	
				else {
					if(xpos)
						var fontSize = xpos;
					else
						var xpos = xdefault;	
				}
			}
		}
		else {
			var xpos = xdefault;
			var fontSize = sizedefault;
		}
	}
	font = String(fontSize*md) + font;
	text = text.replace('^^^REPLACEDAT^^^', '@');
	xpos = width * xpos;
	ypos = height * ypos;
	return { 'font' : font, 'text' : text, 'x': xpos, 'y': ypos };
};
RenderProvider.prototype.drawText = function(srcElement,context,state,detail) {
	if(detail == undefined || detail == null || detail.length == 0)
		return;
	if(!state.color || state.color.length == 0) 
		state.color = ENV.color.default();
	var details = this.parseTextDetail(srcElement,context,detail);
	context.fillStyle = state.color;
	context.textAlign = 'center';
	context.textBaseline = 'ideographic';//i just like the word
	context.font = details.font;
	//alert(details.x + "," + details.y);
	context.fillText(details.text,details.x,details.y);
};
RenderProvider.prototype.parseDetail = function(detailString) {
	var details = [];
	var detailRaw = detailString.split(',');
	for(var dti in detailRaw) 
		details.push(parseFloat(detailRaw[dti].trim()));
	return details;
};
RenderProvider.prototype.drawRect = function(srcElement,context,state,detail) {
	alert('drawRect called');
	var width = srcElement.loc.abs.xsz;
	var w = width;
	var height = srcElement.loc.abs.ysz;
	var h = height;
	var x = srcElement.loc.rel.x;
	var y = srcElement.loc.rel.y;
	if(detail != null && detail != undefined && detail.length > 0) {
		var dx,dy,dw,dh;
		var da = this.parseDetail(detail);
		dx = da[0];
		dy = da[1];
		dw = da[2];
		dh = da[3];
		//alert('details parsed as ' + dx + ',' + dy + ',' + dw + ',' + dh );
		x = x+dx;
		w = dw*width;
		y = y+dy;
		h = dh*height;
	}
	x = x * width;
	y = y * height;
	if(!state.color || state.color.length == 0) {
		state.color = ENV.color.default()
	}
	if(state.fill.is_active()) {
		//alert('fill is active');
		context.fillStyle = state.color;
		context.fillRect(x,y,w,h);
	}
	if(state.stroke.is_active()) {
		//alert('stroke is active');
		context.strokeStyle = state.color;
		context.strokeRect(x,y,w,h);
	}
	if(! state.fill.is_active() && ! state.stroke.is_active()) {
		// default
		//alert('neither stroke nor fill active, defaulting to fill');
		//alert('state color is ' + state.color.length);
		context.fillStyle = state.color;
		//alert('fillstyle is ' + context.fillStyle);
		context.fillRect(x,y,w,h);
	}		
		
};
// E0A765992B
RenderProvider.prototype.drawRoundrect = function(srcElement,context,state,detail) {
// i should outsource this co ordinate code into another function 
	var width = srcElement.loc.abs.xsz;

	var w = width;
	var height = srcElement.loc.abs.ysz;
	var h = height;
	var x = srcElement.loc.rel.x;
	var y = srcElement.loc.rel.y;
	if(detail != null && detail != undefined && detail.length > 0) {
		var dx,dy,dw,dh;
		var da = this.parseDetail(detail);
		dx = da[0];
		dy = da[1];
		dw = da[2];
		dh = da[3];
		//alert('details parsed as ' + dx + ',' + dy + ',' + dw + ',' + dh );
		x = x+dx;
		w = dw*width;
		y = y+dy;
		h = dh*height;
	        var dM = w > h ? w : h;//defaults to 1/16th of maximum dimension
		var dr = dM*1.0/16;  
		if (da.length > 4) // or the 5 dim in the detail if available
			dr = dM*da[4];
	}
	else {
	        var dM = w > h ? w : h;//defaults to 1/16th of maximum dimension
		var dr = dM*1.0/16;  
	}
	x = x * width;
	y = y * height;
	var reduce = Math.ceil(Math.sqrt(2.0)*dr)
	if (reduce > w/2) {
		reduce = w/2;
	}
	if (reduce > h/2) {
		reduce = h/2;
	}
	var cornerNNW = { 'x' : x + reduce, 'y' : y };
	var cornerNWW = { 'x' : x, 'y' : y + reduce };
	var cornerNNE = { 'x' : x + w - reduce, 'y' : y };
	var cornerNEE = { 'x' : x + w, 'y' : y + reduce };
	var cornerSWW = { 'x' : x, 'y' : y + h - reduce };
	var cornerSSW = { 'x' : x + reduce, 'y' : y + h };
	var cornerSEE = { 'x' : x + w, 'y' : y + h - reduce };
	var cornerSSE = { 'x' : x + w - reduce, 'y' : y + h };
	var arcAng = Math.PI/2.0;
	var arcNW = { 'x' : x + reduce, 'y': y + reduce, 'angle' : 2*arcAng };
	var arcNE = { 'x' : x + w - reduce, 'y': y + reduce, 'angle' : 3*arcAng };
	var arcSW = { 'x' : x + reduce, 'y': y + h - reduce, 'angle' : arcAng };
	var arcSE = { 'x' : x + w - reduce, 'y': y + h - reduce, 'angle' : 0 };
	context.beginPath();
     	context.moveTo(cornerNNW.x,cornerNNW.y);
	context.lineTo(cornerNNE.x,cornerNNE.y);
	context.arc(arcNE.x,arcNE.y,reduce,arcNE.angle,arcNE.angle+arcAng);
	context.lineTo(cornerSEE.x,cornerSEE.y);
	context.arc(arcSE.x,arcSE.y,reduce,arcSE.angle,arcSE.angle+arcAng);
	context.lineTo(cornerSSW.x,cornerSSW.y);
	context.arc(arcSW.x,arcSW.y,reduce,arcSW.angle,arcSW.angle+arcAng);
	context.lineTo(cornerNWW.x,cornerNWW.y);
	context.arc(arcNW.x,arcNW.y,reduce,arcNW.angle,arcNW.angle+arcAng);
	if(!state.color || state.color.length == 0) {
		state.color = ENV.color.default()
	}
	if(state.fill.is_active()) {
		//alert('fill is active');
		context.fillStyle = state.color;
		context.fill();
	}
	if(state.stroke.is_active()) {
		//alert('stroke is active');
		context.strokeStyle = state.color;
		context.stroke();
	}
	if(! state.fill.is_active() && ! state.stroke.is_active()) {
		// default
		//alert('neither stroke nor fill active, defaulting to fill');
		//alert('state color is ' + state.color.length);
		context.fillStyle = state.color;
		//alert('fillstyle is ' + context.fillStyle);
		context.fill();
	}
};
// for these ones I need to decide the grammar of detail (json object if avail and then array)
RenderProvider.prototype.drawCircle = function(srcElement,context,state,detail) {

};
RenderProvider.prototype.drawSquare = function(srcElement,context,state,detail) {

};
RenderProvider.prototype.drawLine = function(srcElement,context,state,detail) {

};
RenderProvider.prototype.drawPath = function(srcElement,context,state,detail) {

};
function ColorSource() {
	return this;

};
ColorSource.prototype.orange = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	//alert('returning orange with alpha ' + alpha );
	return 'hsla(30,100%,50%,' + alpha + ')';
};
ColorSource.prototype.blue = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(200,100%,50%,' + alpha + ')';
};
ColorSource.prototype.red = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(5,100%,50%,' + alpha + ')';
};
ColorSource.prototype.yellow = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(90,100%,50%,' + alpha + ')';
};
ColorSource.prototype.green = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(130,100%,50%,' + alpha + ')';
};
ColorSource.prototype.black = function(alpha) {
	if(!alpha || alpha.length == 0)
		alpha = 1.0;
	return 'hsla(0,0%,0%,' + alpha + ')';
};
ColorSource.prototype.default = function() {
	return this.orange(toArray(arguments));
};
var ENV;
ENV = {
	'main': goLive(requestDistinctOnscreenContext()),
	'color': new ColorSource(),
	'renderProvider': new RenderProvider()
};
function Render() {
	this.provider = ENV.renderProvider;
	this.renderactions = [];
	return this;
};
Render.prototype.fill = function () {
	this.renderactions.push(new RenderAction('fill'));
	return this;
};
Render.prototype.nofill = function () {
	this.renderactions.push(new RenderAction('nofill'));
	return this;
}
Render.prototype.stroke = function () {
	this.renderactions.push(new RenderAction('stroke'));
	return this;
};
Render.prototype.nostroke = function () {
	this.renderactions.push(new RenderAction('nostroke'));
	return this;
};
Render.prototype.color = function (color) {
	this.renderactions.push(new RenderAction('color',color));
	return this;
};
Render.prototype.rect = function (detail) {
	this.renderactions.push(new RenderAction('rect',detail));
	return this;
};
Render.prototype.circle = function (detail) {
	this.renderactions.push(new RenderAction('circle',detail));
	return this;
};
Render.prototype.roundrect = function (detail) {
	this.renderactions.push(new RenderAction('roundrect',detail));
	return this;
};
Render.prototype.square = function (detail) {
	this.renderactions.push(new RenderAction('square',detail));
	return this;
};
Render.prototype.text = function(text) {
	this.renderactions.push(new RenderAction('text',text));
	return this;
};
// image is special we need to wait for load
Render.prototype.img = function(img) {
	this.renderactions.push(new RenderAction('img',img));
	return this;
};
Render.prototype.line = function(line) {
	this.renderactions.push(new RenderAction('line',line));
	return this;
};
Render.prototype.path = function(path) {
	this.renderactions.push(new RenderAction('path',path));
	return this;
};
function State() {
	this.state = 0;
	return this;
};
State.prototype.active = function() {
	this.state = 1;
};
State.prototype.inactive = function() {
	this.state = 0;
};
State.prototype.is_inactive = function() {
	return (this.state == 0);
};
State.prototype.is_active = function() {
	return (this.state == 1);
};
function RenderState(oState) {
	if(oState) {
		this.stroke = oState.stroke;
		this.fill = oState.fill;
		this.color = oState.color;
		this.image = oState.image;
	}
	else {
		this.stroke = new State();
		this.fill = new State();
		this.color = '';
		this.image = new State();
	}
	return this;
};
RenderState.prototype.is_blank = function() {
	return (this.stroke.is_inactive() && this.fill.is_inactive() && this.color == '' && this.image.is_inactive());	
};
function RenderAction(name,detail) {
	this.name = name;
	this.detail = detail;
	return this;
};
// the api is coming along ideally i should wrap it all up in a canvas class wrapper or something
// but actually probably best to just keep developing now
// it looks great. so fucking awesome
Render.prototype.renderTo = function(srcElement, context,oncomplete) {
	var state = new RenderState();
	var final_state = state;
	var renderq = [];
	//alert('RenderTo called, renderactions : ' + this.renderactions.length);
	for(var ra in this.renderactions) {
		var action = this.renderactions[ra];
		//alert('action is ' + action );
		switch(action.name) {
			case 'fill':
				state.fill.active();
				break;
			case 'stroke':
				state.stroke.active();
			break;
			case 'nofill':
				state.fill.inactive();
				break;
			case 'nostroke':
				state.stroke.inactive();
				break;
			case 'color':
				state.color = action.detail;
				break;
			case 'img':
				state.image.active();
				renderq.push([action,new RenderState(state)]);
				if(!state.is_blank())
					final_state = state;
				state = new RenderState();
				break;
			case 'rect':
				renderq.push([action,new RenderState(state)]);
				if(!state.is_blank())
					final_state = state;
				state = new RenderState();
				break;
			case 'circle':
				renderq.push([action,new RenderState(state)]);
				if(!state.is_blank())
					final_state = state;
				state = new RenderState();
				break;
			case 'roundrect':
				renderq.push([action,new RenderState(state)]);
				if(!state.is_blank())
					final_state = state;
				state = new RenderState();
				break;
			case 'square':
				renderq.push([action,new RenderState(state)]);
				if(!state.is_blank())
					final_state = state;
				state = new RenderState();
				break;
			case 'text':
				renderq.push([action,new RenderState(state)]);
				if(!state.is_blank())
					final_state = state;
				state = new RenderState();
				break;
			case 'line':
				renderq.push([action,new RenderState(state)]);
				if(!state.is_blank())
					final_state = state;
				state = new RenderState();
				break;
			case 'path':
				renderq.push([action,new RenderState(state)]);
				if(!state.is_blank())
					final_state = state;
				state = new RenderState();
				break;
			default:
				break;	
		}
	}
	// process render q
	var that = this;
	for (var qi in renderq) {
		var rendercall;
		var actionstate = renderq[qi];
		var action = actionstate[0];
		var wstate = actionstate[1];
		//alert('action.name ' + action.name + ' received');
		//this is not really working
		if (wstate.is_blank() && !final_state.is_blank()) {
			wstate = final_state;		
			//alert('used final wstate');
		}
		switch(action.name) {
			case 'rect':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						//alert('Drawing rect');
						that.provider.drawRect(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'circle':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						//alert('Drawing circle');
						that.provider.drawCircle(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;			
			case 'roundrect':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						//alert('Drawing roundrect');
						that.provider.drawRoundrect(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'square':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						//alert('Drawing square');
						that.provider.drawSquare(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'text':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						//alert('Drawing text');
						that.provider.drawText(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'img':
				var loadhook = function () { 
					//alert('image finished loading, on q:' + waitq.length);
					srcElement.imageloading -= 1;
					//alert("in loadhook" +srcElement.imageloading);
					//alert('Onload');

					//alert('q len ' + waitq.length);
					while(srcElement.waitq.length > 0) {
						var q = srcElement.waitq.pop();
						//alert('About to render, remaining ' + waitq.length + ','+ q.rendercall);
						if(q.wstate.image.is_active()) {
							//alert('Image again');
							srcElement.imageloading += 1;
							//alert("in waitq" + srcElement.imageloading);
							q.rendercall();
							return;
						}
						q.rendercall();
					}
					//alert('Hook complete calling ' + oncomplete);
					
					if(srcElement.imageloading == 0) 
						oncomplete();
				};
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail,loadhooki) {
					return function () {
						//alert('Drawing image');
						that.provider.drawImage(srcElementc,contextc,wstatec,actioncdetail,loadhooki);
					};
				})(srcElement,context,wstate,action.detail,loadhook);
				break;
			case 'line':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						//alert('Drawing line');
						that.provider.drawLine(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			case 'path':
				rendercall = (function (srcElementc,contextc,wstatec,actioncdetail) {
					return function () {
						//alert('Drawing path');
						that.provider.drawPath(srcElementc,contextc,wstatec,actioncdetail);
					};
				})(srcElement,context,wstate,action.detail);
				break;
			default:
				break;
		}
		if (srcElement.imageloading > 0) {
			//alert("at switch end" +srcElement.imageloading);
			//alert('An image is loading, pushing ' + wstate.image.is_active() + ',' + rendercall);
			srcElement.waitq.push({ 'wstate': wstate, 'rendercall' : rendercall});
			continue;
		}
		else if (wstate.image.is_active()) {
			//alert('An image is about to start loading '+ rendercall);
			srcElement.imageloading += 1;
			//alert("an image found" +srcElement.imageloading);
		}
		rendercall();			 
	}
	if(srcElement.imageloading == 0) {
		//alert("== 0 " + srcElement.imageloading);
		//alert('q len ' + waitq.length);
		while(srcElement.waitq.length > 0) {
			var q = srcElement.waitq.pop();
			if(q.state.image.is_active()) {
				srcElement.imageloading += 1;
				q.rendercall();
				return;
			}
			q.rendercall();
		}
		//alert('Post complete calling ' + oncomplete);
		srcElement.launchable = 1;
		if(srcElement.imageloading == 0)
			oncomplete();
	}
	
};
window._ = function () {
	return new Render();
};
window.Color = ENV.color;

window.onload = function () {
	var wc = new Free();
	var renderact3 = _().color(Color.black(0.2)).roundrect().color(Color.green()).roundrect('0.5,0.5,0.2,0.2').color(Color.blue(0.5)).roundrect('0.5,0.8,1.0,0.2');
	var renderact4 = _().img('Star-Field.gif').color().text('Hi'); 
	var renderact5 = _().img('t.jpg').color(Color.blue()).text('@ 0.5 I rock');
// if no size set just use the size set previusly in this render, or if none ever set the default. Everything aftr the @ is where on the screen. Everything else is taken care. If no position specified just default to center.
	//var renderact4 = _().img('t.jpg');
	// i should inline this la callback to come from wc so as to not need to argument it 
	// i need to handle these oncomplete functions better	
// should design a funcion chain to allow .color().green(). instead
	function la () {
		wc.launch.call(wc);
	}
	function ja () {

	}
	// some issues we need a way for states to persist if they have color or such information -- this currently does not work
	// plus we need a way for oncomplete functions, or images loads to persist across calls to render, otherwise we break the delicate state created by oncomplete rendering and we lost the delicate draw order that we currently preserve. so imageloading needs to be a property of the source object, or of an eternal renderprovider or something like that. we need ways to persist a state across within a render call chain and also a way to persist image loading awareness and oncomplete coherence across calls to renders
	// and for some reason we must draw all renders at the end of every renders call, this causes doubling up, which is not cool 
	wc.renders(renderact5,la);
	//wc.draw();
	wc.renders(renderact3,la);
	wc.draw();
	//wc.renders(renderact3,la);
	//alert('Wait for it...');
	//wc.launch();
	// basically it is consistent with image onload
	// it's just that i was stuffing it up by calling draw
	// and drawing the non ready , i.e. empty off screen canvas
	// before the image was ready. 
	// so we need to dance to the timing of image onload
	// if we do that it is ok
	// needed: an elegant way to do callback using image onload
	// to fire the launch if everything else is done
	// or an elegant way to allow draw and launch to wait for image to 
	// complete loading.
	//each Element needs a kind of checking function
	// to check when the draw to the canvas actually complete
	// this means sampling the bottom right hand corner.
	// a tiny piece, and getting the image data for that piece and checking if pixels are turned on
};
